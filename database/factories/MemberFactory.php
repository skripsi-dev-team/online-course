<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Member;
use Faker\Generator as Faker;

$factory->define(Member::class, function (Faker $faker) {
    return [
        'name'  => $faker->name,
        'email' => $faker->safeEmail,
        'password'  => bcrypt('secret'),
        'address'    => $faker->address,
        'phone'   => $faker->phoneNumber,
        'email_verified_at' => null
    ];
});
