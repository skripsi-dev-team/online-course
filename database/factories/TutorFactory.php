<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Tutor;
use Faker\Generator as Faker;

$factory->define(Tutor::class, function (Faker $faker) {
    return [
        'name'  => $faker->name,
        'username'  => $faker->userName,
        'email' => $faker->safeEmail,
        'password'  => 'secret',
        'address'    => $faker->address,
        'phone'   => $faker->phoneNumber,
        'role'      => 0
    ];
});
