<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'category_name'    => $faker->sentence($nbWords = 2, $variableNbWords = true),
        'description'     => $faker->paragraph(5),
        'file_pdf'      => 'EANY0OhHQZNlMav1nhYpIEWJtPCJC7B6YGXs84CQ.docx',
        'packet_id'      => function() {
            return App\Packet::all()->random()->id;
        }
    ];
});
