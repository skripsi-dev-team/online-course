<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Packet;
use Faker\Generator as Faker;

$factory->define(Packet::class, function (Faker $faker) {
    return [
        'packet_name'    => $faker->word,
        'packet_price'         => $faker->numberBetween(100000, 1000000),
        'description'     => $faker->paragraph(2),
        'number_of_categories'   => 0,
        'tutor_id'  => function() {
            return App\Tutor::all()->random()->id;
        }
    ];
});
