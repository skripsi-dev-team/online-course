<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Video;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Video::class, function (Faker $faker) {
    $title = $faker->sentence($nbWords = 4, $variableNbWords = true);
    return [
        'title' => $title,
        'slug' => str_replace(" ","-",$title),
        'embed_link' => "https://www.youtube.com/embed/Ik-DhHJM8eo&t=34s",
        'description' => $faker->paragraph(5),
        'category_id' => 9
    ];
});
