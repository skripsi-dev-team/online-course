<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MemberTableSeeder::class);
        $this->call(TutorTableSeeder::class);
        $this->call(PacketSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(VideoSeeder::class);
    }
}
