<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersPacketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members_packets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member_id')->unsigned();
            $table->bigInteger('paket_id')->unsigned();
            $table->string('kode_akses');
            $table->tinyInteger('aktivasi');
            $table->tinyInteger('status');
            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('paket_id')->references('id')->on('packets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members_packets');
    }
}
