<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldPackets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packets', function(Blueprint $table){
            $table->renameColumn('nama_paket', 'packet_name');
            $table->renameColumn('deskripsi', 'description');
            $table->renameColumn('jumlah_kategori', 'number_of_categories');
            $table->renameColumn('harga_paket', 'packet_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
