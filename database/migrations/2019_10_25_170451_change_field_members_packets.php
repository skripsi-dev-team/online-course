<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldMembersPackets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members_packets', function(Blueprint $table){
            $table->renameColumn('paket_id', 'packet_id');
            $table->renameColumn('kode_akses', 'access_code');
            $table->renameColumn('aktivasi', 'activation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
