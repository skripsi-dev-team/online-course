<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('komentar');
            $table->bigInteger('member_id')->unsigned()->nullable();
            $table->bigInteger('video_id')->unsigned();
            $table->dateTime('tanggal');
            $table->bigInteger('tutor_id')->unsigned()->nullable();
            $table->bigInteger('reply_id')->default(0);
            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('video_id')->references('id')->on('videos')->onDelete('cascade');
            $table->foreign('tutor_id')->references('id')->on('tutors')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
