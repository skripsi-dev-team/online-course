<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = [
        'file_exam',
        'description',
        'date',
        'packet_id'
    ];

    public function packet(){
        return $this->belongsTo(Packet::class, 'packet_id');
    }

    public function answerExams(){
        return $this->hasMany(AnswerExam::class, 'exam_id');
    }
}
