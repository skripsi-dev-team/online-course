<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'category_name',
        'number_ofvideos',
        'description',
        'packet_id',
        'file_pdf'
    ];

    public function packet() {
        return $this->belongsTo(Packet::class, 'packet_id');
    }

    public function progresses() {
        return $this->belongsToMany(Member::class, 'progresses', 'category_id', 'member_id');
    }

    public function videos() {
        return $this->hasMany(Video::class, 'category_id');
    }

    public function isLatestCategory() {
        $latestCategory = $this->packet->categories()->orderBy('created_at', 'desc')->first();
        
        if ($this->id == $latestCategory->id) {
            return true;
        }

        return false;
    }
}
