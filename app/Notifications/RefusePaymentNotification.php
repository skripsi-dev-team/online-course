<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RefusePaymentNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $member;
    private $packet;

    public function __construct($member, $packet)
    {
        $this->member = $member;
        $this->packet = $packet;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Pembayaran Gagal')
                    ->from($this->packet->tutor->email)
                    ->greeting('Pembayaran Ditolak!')
                    ->line('Halo, '.$this->member->name.',')
                    ->line('Mohon maaf pembayaran anda untuk paket '.$this->packet->packet_name.' dengan harga Rp. ' .number_format($this->packet->packet_price) . ' kami tolak dikarenakan bukti pembayaran tidak valid')
                    ->line('Silahkan upload kembali bukti pembayaran, untuk melakukan aktivasi paket ini.')
                    ->line('Terima kasih!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
