<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendSertifikatNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $member;
    private $paket;
    private $certificate_number;
    private $url;
    public function __construct($member, $paket, $certificate_number, $url)
    {
        $this->member = $member;
        $this->paket = $paket;
        $this->certificate_number = $certificate_number;
        $this->url = $url;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("Sertifikat Penyelesaian Kursus")
                    ->greeting('Selamat '.$this->member->name)
                    ->line('Anda telah menyelesaikan kursus '. $this->paket->packet_name)
                    ->line('Berikut link untuk mendownload sertifikat')
                    ->action('Download Sertifikat', $this->url)
                    ->line('Terima Kasih!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
