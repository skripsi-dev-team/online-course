<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MemberRegisterWithPacket extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $member;
    private $packet;

    public function __construct($member, $packet)
    {
        $this->member = $member;
        $this->packet = $packet;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Registrasi Berhasil')
                    ->from($this->packet->tutor->email)
                    ->greeting('Selamat Bergabung!')
                    ->line('Halo, '.$this->member->name.' anda telah berhasil mendaftar.')
                    ->line('Paket yang anda daftarkan adalah '.$this->packet->packet_name.' dengan harga Rp. ' .number_format($this->packet->packet_price))
                    ->line('Untuk melakukan pembayaran transfer ke rekening BCA 0820820 a/n Atas Nama TAW Course')
                    ->line('Silahkan lakukan pembayaran lalu reply email ini dengan bukti transfer.')
                    ->line('Terima kasih!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
