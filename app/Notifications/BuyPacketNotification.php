<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class BuyPacketNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $member;
    private $tutor;

    public function __construct($tutor, $member)
    {
        $this->member = $member;
        $this->tutor = $tutor;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Halo, '.$this->member->name)
                    ->line('Terima kasih telah melakukan permintaan penambahan paket.')
                    ->line('Silahkan untuk melakukan pendaftaran dengan transfer ke rekening BCA 0820820 a/n Atas Nama TAW Course')
                    ->line('Jika sudah melakukan pembayaran, silahkan reply email ini dengan menyertakan bukti transfer')
                    ->line('Terima kasih atas perhatiannya')
                    ->from($this->tutor->tutor->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
