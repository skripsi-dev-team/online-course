<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use DB;

class SendKodeNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $member;
    private $paket;
    public function __construct($member, $paket)
    {
        $this->member = $member;
        $this->paket = $paket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $access_code = DB::table('members_packets')->where('packet_id', $this->paket->id)->where('member_id', $this->member->id)->first();
        //dd($access_code->access_code);
        return (new MailMessage)
                    ->from($this->paket->tutor->email)
                    ->subject('Kode Akses '.$this->paket->packet_name)
                    ->line('Halo '.$this->member->name. ', pembayaran anda telah dikonfirmasi.')
                    ->line('Berikut kode akses untuk mengaktifkan paket: ')
                    ->line($access_code->access_code)
                    ->line('Silahkan login dan masukan kode akses diatas pada paket yang sudah anda beli.')
                    ->action('Aktifkan Paket', url('member/paket-saya/'))
                    ->line('Terima kasih!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
