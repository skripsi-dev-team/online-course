<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamAnswer extends Model
{
    protected $fillable = [
        'exam_id',
        'member_id',
        'description',
        'file_answer_exam',
        'date',
        'score',
        'tutor_description'
    ];

    public function member(){
        return $this->belongsTo(Member::class, 'member_id');
    }

    public function exam(){
        return $this->belongsTo(Exam::class, 'exam_id');
    }

    public function scoreStatus(){
        if($this->score >= 50){
            return "<span class='badge badge-success'>Lulus</span>";
        }  else {
            return "<span class='badge badge-danger'>Tidak Lulus</span>";
        }
    }
}
