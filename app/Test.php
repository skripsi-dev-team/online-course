<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = [
        'file_test',
        'description',
        'date',
        'category_id',
    ];

    public function category() {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function answers() {
        return $this->hasMany(Answer::class, 'test_id');
    }

}
