<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sertificate extends Model
{
    protected $fillable = [
        'certificate_number',
        'date',
        'member_id',
        'packet_id'
    ];

    public function member() {
        return $this->belongsTo(Member::class);
    }

    public function packet(){
        return $this->belongsTo(Packet::class);
    }
}
