<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'test_id',
        'member_id',
        'description',
        'file_answer',
        'date',
        'score',
        'tutor_description'
    ];

    public function member(){
        return $this->belongsTo(Member::class, 'member_id');
    }

    public function test(){
        return $this->belongsTo(Test::class, 'test_id');
    }

    public function scoreStatus(){
        if($this->score >= 50){
            return "<span class='badge badge-success'>Lulus</span>";
        }  else {
            return "<span class='badge badge-danger'>Tidak Lulus</span>";
        }
    }
}
