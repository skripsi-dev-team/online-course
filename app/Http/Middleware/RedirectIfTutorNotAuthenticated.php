<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfTutorNotAuthenticated extends Authenticate
{
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return route('tutor.login-form');
        }
    }
}
