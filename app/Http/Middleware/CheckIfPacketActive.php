<?php

namespace App\Http\Middleware;

use Closure;
use App\Packet;
use Auth;
use DB;
use App\Member;

class CheckIfPacketActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->route('id');
        $user = Auth::user();
        //$active = Member::find($user->id)->progresses()->withPivot('pass')->get();
        
        $packet = Packet::whereHas('members', function($q) use($user) {
            $q->where('members.id', $user->id);
        })
        ->findOrFail($id);
        
        $pivot = $packet->members()->withPivot('activation')->where('member_id', $user->id)->first();

        if ($pivot->pivot->activation == 0) {
            return redirect()->back();
        }
        return $next($request);
    }
}
