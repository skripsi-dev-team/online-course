<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfMemberNotAuthenticated extends Authenticate
{
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return route('member.login-form');
        }
    }
}
