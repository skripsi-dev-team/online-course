<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\MemberChatSended;
use App\Events\TutorChatSended;
use App\Member;
use App\Packet;
use App\Tutor;

class ChatApiController extends Controller
{
    public function sendChat(Member $member, Packet $packet, Request $request)
    {
        event(new MemberChatSended($member, $packet->tutor, $request->get('message')));

        return response()->json([
            'status' => 'ok',
            'message' => 'chat sended!'
        ], 200);
    }

    public function sendChatToMember(Tutor $tutor, Member $member, Request $request)
    {
        event(new TutorChatSended($tutor, $member, $request->get('message')));

        return response()->json([
            'status' => 'ok',
            'message' => 'chat sended!'
        ], 200);
    }

    public function loadHistoryForTutor(Tutor $tutor, Member $member)
    {
        $chats = $tutor->chats()
                        ->wherePivot('member_id', $member->id)
                        ->withPivot(['chat', 'sent_to'])
                        ->orderBy('date')
                        ->get();


        $histories = array();

        foreach ($chats as $chat) {
            if ($chat->pivot->sent_to == $tutor->id) {
                $histories[] = [
                    'text' => $chat->pivot->chat,
                    'type' => 'left'
                ];
            }else {
                $histories[] = [
                    'text' => $chat->pivot->chat,
                    'type' => 'right'
                ];
            }    
        }

        return response()->json([
            'data' => [
                'tutor' => $tutor,
                'histories' => $histories
            ]
        ], 200);
    }

    public function loadHistoryForMember(Member $member, Tutor $tutor)
    {
        $chats = $member->chats()
                        ->wherePivot('tutor_id', $tutor->id)
                        ->withPivot(['chat', 'sent_to'])
                        ->orderBy('date')
                        ->get();


        $histories = array();

        foreach ($chats as $chat) {
            if ($chat->pivot->sent_to == $member->id) {
                $histories[] = [
                    'text' => $chat->pivot->chat,
                    'type' => 'left'
                ];
            }else {
                $histories[] = [
                    'text' => $chat->pivot->chat,
                    'type' => 'right'
                ];
            }    
        }

        return response()->json([
            'data' => [
                'member' => $member,
                'histories' => $histories
            ]
        ], 200);
    }
}
