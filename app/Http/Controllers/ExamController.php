<?php

namespace App\Http\Controllers;

use App\Exam;
use App\ExamAnswer;
use App\Packet;
use Illuminate\Http\Request;
use PDF;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($packet_id)
    {
        $data['packet'] = Packet::find($packet_id);
        $data['exam'] = Exam::where('packet_id', $packet_id)->first();
        return view('tutor.exam.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($packet_id)
    {
        $data['packet'] =  packet::find($packet_id);
        return view('tutor.exam.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($packet_id, Request $request)
    {
        $this->validate($request, [
            'file_exam' => 'required',
            'description' => 'required',
        ]);

        $store_file = $request->file('file_exam')->store('file_exam');

        Exam::create([
            'file_exam' => $request->file_exam->hashName(),
            'description' => $request->description,
            'date' => date('Y-m-d H:i:s'),
            'packet_id' => $packet_id
        ]);

        return redirect()->route('paket.index')->with('success', 'Berhasil menambah file ujian');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['exam'] = Exam::find($id);
        $data['packet'] = Packet::find($data['exam']->packet_id);
        return view('tutor.exam.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exam = Exam::find($id);
        $file_exam = $exam->file_exam;
        if($request->hasFile('file_exam') == true){
            unlink('storage/file_exam/'.$file_exam);
            $request->file('file_exam')->store('file_exam');
            $file_exam = $request->file_exam->hashName();
        }

        $exam->update([
            'description' => $request->description,
            'date' => date('Y-m-d H:i:s'),
            'file_exam' => $file_exam,
        ]);

        return redirect()->route('exam.index', ['id' => $exam->packet_id])->with('success', 'Berhasil mengedit file test');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function answerExam($id){
        $data['exam'] = Exam::findOrFail($id);
        $data['answers'] = ExamAnswer::where('exam_id', $id)->get();
        return view('tutor.exam.answer', $data);
    }

    public function setNilaiExam(Request $request){
        $answer = ExamAnswer::find($request->id);
        $exam = $answer->exam;
        $answer->update(['score' => $request->nilai, 'tutor_description' => $request->tutor_description]);
        $member_id = $request->member_id;
    
        return redirect()->back()->with('success', 'Berhasil memberi nilai ujian pada member tersebut');
        
    }


    public function nilaiUjian(){
        $data['nilai'] = ExamAnswer::join('members', 'members.id', '=', 'exam_answers.member_id')
                                    ->orderBy('members.name', 'asc')
                                    ->get();

        return view('tutor.nilai.nilai_ujian', $data);
    }

    public function printUjian(){
        $data['nilai'] = ExamAnswer::join('members', 'members.id', '=', 'exam_answers.member_id')
                                    ->orderBy('members.name', 'asc')
                                    ->get();

        $pdf = PDF::loadView('tutor.nilai.print_ujian', $data);
        return $pdf->download('nilai-ujian-member.pdf');
    }
}
