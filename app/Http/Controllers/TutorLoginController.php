<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class TutorLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = 'tutor/dashboard';

    public function __construct()
    {
        $this->middleware('guest:tutor')->except('logout');
    }

    public function showLoginForm()
    {
        return view('tutor.auth.login');
    }

    public function dashboard()
    {
        return view('tutor.dashboard');
    }

    protected function guard()
    {
        return Auth::guard('tutor');
    }
}
