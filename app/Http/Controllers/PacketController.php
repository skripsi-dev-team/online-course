<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Packet;
use App\Tutor;
use App\Category;
use App\Member;
use App\Notifications\RefusePaymentNotification;
use Auth;
use DB;

class PacketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $packet;

    public function __construct()
    {
        $this->packet = new Packet();    
    }

    public function index()
    {
        $user = Auth::user();
        $data['packets'] = $user->packets;

        return view('tutor.packet.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['tutors'] = Tutor::all();
        return view('tutor.packet.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'packet_name' => 'required',
            'packet_price' => 'required',
            'description' => 'required'  
        ]);

        $insert = $user->packets()->create($request->all());

        return redirect()->route('exam.index', ['packet_id' => $insert->id])->with('success', 'Berhasil menambahkan paket, sekarang tambahkan ujian pada paket tersebut');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['packet'] = $this->packet->find($id);
        $data['categories'] = Category::where('packet_id', $id)->get();
        return response()->json([
            'data' => [
                'packet' => $data['packet'],
                'categories' => $data['categories']
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['packet'] = $this->packet->find($id);
        $data['categories'] = $this->packet->find($id)->categories()->get();
        return view('tutor.packet.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $this->validate($request, [
            'packet_name' => 'required',
            'packet_price' => 'required',
            'description' => 'required'  
        ]);

        $user->packets()->find($id)->update($request->all());

        return redirect()->route('paket.index')->with('success', 'Berhasil mengedit paket');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->packet->find($id)->delete();
        return redirect()->route('paket.index')->with('success', 'Berhasil menghapus paket');
    }

    
    public function indexMember(){
        $data['packets'] = Packet::has('categories')->get();

        return view('member.packet.all_packets', $data);
    }

    public function payment(){        
        $tutor = Packet::where('tutor_id', Auth::user()->id)->get();
        $data['packets'] = $tutor;
            
        return view('tutor.payment.index', $data);
    }

    public function deletePayment(Request $request, $id){
        $data = DB::table('members_packets')->where('id', $id)->first();
        $member = Member::find($data->member_id);
        $packet = Packet::find($data->packet_id);
 
        if(is_file('storage/file_receipt/'.$data->payment_receipt)){
            unlink('storage/file_receipt/'.$data->payment_receipt);
        }

        DB::table('members_packets')->where('id', $id)->update([
            'payment_date' => null,
            'payment_receipt' => null
        ]);

        $member->notify(new RefusePaymentNotification($member, $packet));

        return redirect()->back()->with('success', 'Berhasil menghapus data pembayaran');
           
        
    }


    
}
