<?php

namespace App\Http\Controllers;
use App\Answer;
use App\Tutor;
use App\Category;
use App\Notifications\AnswerNotification;
use App\Notifications\SendSertifikatNotification;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
Use Auth;
use App\Member;
use App\Packet;
use App\Sertificate;
use PDF;

class AnswerController extends Controller
{
    public function create($packet_id, $category_id, Request $request){
        $this->validate($request, [
            'file_answer' => 'required',
        ]);
        
        if($request->jawaban_id == null || $request->jawaban_id == ''){
            $request->file('file_answer')->store('file_answer');
            $answer = Answer::create([
                'test_id' => $request->test_id,
                'member_id' => Auth::guard('member')->user()->id,
                'description' => $request->description,
                'file_answer' => $request->file_answer->hashName(),
                'date' => date('Y-m-d H:i:s'),
            ]);
        } else {
            $answer = Answer::find($request->jawaban_id);

            if(is_file('storage/file_answer/'.$answer->file_answer)){
                unlink('storage/file_answer/'.$answer->file_answer);
            }

            $request->file('file_answer')->store('file_answer');

            $answer->update([
                'test_id' => $request->test_id,
                'member_id' => Auth::guard('member')->user()->id,
                'description' => $request->description,
                'file_answer' => $request->file_answer->hashName(),
                'date' => date('Y-m-d H:i:s'),
            ]);
        }


        $tutor = $answer->test->category->packet->tutor;
        
        $tutor->notify(new AnswerNotification($tutor, $answer));
        $tutor->notifications()->create([
            'title' => 'Jawaban Test Baru',
            'text' => 'Anda mendapat jawaban baru dari seorang member pada kategori '.$answer->test->category->category_name,
            'seen' => 0,
            'date' => now(),
            'url' => '/tutor/kategori/test/'.$answer->test->id.'/answer'
        ]);
        
        return redirect()->back()->with('success', 'Berhasil mengsubmit jawaban!');
    }

    public function setNilai(Request $request){
        $answer = Answer::find($request->id);
        $test = $answer->test;
        $answer->update(['score' => $request->nilai, 'tutor_description' => $request->tutor_description]);
        $member_id = $request->member_id;
        if($request->nilai > 50){
            $progress = $answer->test->category;
            $packet = $answer->test->category->packet;
            $progress->progresses()->updateExistingPivot($answer->member->id, ['pass' => 1]);
            $category = $packet->categories()->whereDoesntHave('progresses', function($q) use($member_id) { 
                $q->where('member_id', $member_id);
            })->first();
            
            if($category != null){
            
                if ($test->answers->where('member_id', $member_id)->count() == 1) {
                    $category->progresses()->attach($answer->member->id, ['pass' => 0]);
                }
            }
            return redirect()->back()->with('success', 'Berhasil memberi nilai member tersebut');
        } else {
            return redirect()->back()->with('success', 'Berhasil memberi nilai member tersebut');
        }
    }


    public function generateSertifikat($member_id, $packet_id, $certificate_number){
        //dd($request->all());
        $data['member'] = Member::find($member_id);
        $data['packet'] = Packet::find($packet_id);
        $data['certificate_number'] = $certificate_number;


    
        $pdf = PDF::loadView('tutor.sertifikat.generate', $data);
        return $pdf->setPaper('a4', 'potrait')->download('sertifikat.pdf');
    }

    public function sendSertifikat($member_id, $packet_id){
        $data['member'] = Member::find($member_id);
        $data['packet'] = Packet::find($packet_id);

        $certificate_number = 'SK-'.strtotime('now').'-'.date('Ymd');
        $certificate = Sertificate::create([
            'certificate_number' => $certificate_number,
            'member_id' => $member_id,
            'packet_id' => $packet_id,
            'date' => date('Y-m-d')
        ]);

      

        $data['member']->notify(new SendSertifikatNotification($data['member'], $data['packet'], $certificate_number, route('tutor.generate-sertifikat', ['member_id' => $member_id, 'packet_id' => $packet_id, 'certificate_number' => $certificate_number])));
        return redirect()->back()->with('success', 'Berhasil Mengirim sertifikat ke email Member');
    }



    /* Nilai Test */

    public function nilaiTest() 
    {
        $data['nilai'] = Answer::join('members', 'members.id', '=', 'answers.member_id')
                                ->orderBy('members.name', 'asc')
                                ->get();
        return view('tutor.nilai.nilai_test', $data);
    }

    public function printNilai(){
        $data['nilai'] = Answer::join('members', 'members.id', '=', 'answers.member_id')
                                ->orderBy('members.name', 'asc')
                                ->get();
        $pdf = PDF::loadView('tutor.nilai.print_test', $data);
        return $pdf->download('nilai-test-member.pdf');
    }
}
