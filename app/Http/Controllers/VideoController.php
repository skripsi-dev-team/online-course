<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;
use App\Category;
use App\Http\Requests\StoreVideo;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_kategori)
    {
        $data['videos'] = Video::where('category_id', $id_kategori)->get();
        $data['category'] = Category::find($id_kategori);
        return view('tutor.video.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_kategori)
    {
        return view('tutor.video.create')->with('id_kategori', $id_kategori);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVideo $request, $id_kategori)
    {
        $video = new Video();
        $video->create([
            'title' => $request->title,
            'slug' => $request->slug,
            'embed_link' => $request->embed_link,
            'description' => $request->description,
            'category_id' => $request->category_id
        ]);

        return redirect()->route('video.index', ['id' => $request->category_id])->with('success', 'Berhasil menambah data video');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_kategori, $id)
    {
        return Video::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_kategori, $id)
    {
        $data['video'] = Video::find($id);
        $data['id_kategori'] = $id_kategori;
        return view('tutor.video.edit', $data);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreVideo $request, $id_kategori, $id)
    {
        $video = Video::find($id);

        $video->update([
            'title' => $request->title,
            'slug' => $request->slug,
            'embed_link' => $request->embed_link,
            'description' => $request->description,
            'category_id' => $request->category_id
        ]);

        return redirect()->route('video.index', ['id' => $request->category_id])->with('success', 'Berhasil mengedit data video');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $video_id)
    {
        
        $video = Video::find($video_id);
        $video->delete();

        return redirect()->route('video.index', ['id' => $id])->with('success', 'Berhasil menghapus data video'); 
    }
}
