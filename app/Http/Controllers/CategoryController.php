<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Packet;
use App\Video;
use App\Test;
use App\Http\Requests\StoreCategory;
use App\Tutor;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $packet;

    public function __construct()
    {
        $this->packet = new Packet();
    }

    public function index()
    {
        $user = Auth::user();
        $data['categories'] = $user->categories;
        return view('tutor.category.index', $data);
    }

    public function view($category_id){
        $user = Auth::user();
        $data['categories'] = $user->categories->find($category_id);
        $data['videos'] = Video::where('category_id', $category_id)->get();
        $data['tests'] = Test::where('category_id', $category_id)->first();
        return view('tutor.category.view', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $data['packets'] = $user->packets;
        $data['tutors'] = Tutor::all();
        return view('tutor.category.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategory $request)
    {
        $store_file = $request->file('file_pdf')->store('file_pdf');
        
        $insert = Category::create([
            'category_name' => $request->category_name,
            'description' => $request->description,
            'file_pdf' => $request->file_pdf->hashName(),
            'packet_id' => $request->packet_id
        ]);

        return redirect()->route('test.create', ['id_kategori' => $insert->id])->with('success', 'Berhasil menambah data kategori, silahkan tambah test');
    }

    public function storePacket(Request $request){
        $this->validate($request, [
            'packet_name' => 'required',
            'packet_price' => 'required',
            'description' => 'required',
            'tutor_id' => 'required|numeric'  
        ]);
        
        $this->packet->packet_name = $request->packet_name;
        $this->packet->packet_price = $request->packet_price;
        $this->packet->description = $request->description;
        $this->packet->tutor_id = $request->tutor_id;
        $this->packet->number_of_categories = 0;
        $this->packet->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Category::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $data['category'] = Category::find($id);
        $data['packets'] = $user->packets;
        $data['videos'] = Category::find($id)->videos()->get();
        return view('tutor.category.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCategory $request, $id)
    {   
        $category = Category::find($id);
        $file_name = $category->file_pdf;
        if($request->hasFile('file_pdf') == true){
            unlink('storage/file_pdf/'.$file_name);
            $request->file('file_pdf')->store('file_pdf');
            $file_name = $request->file_pdf->hashName();
        }

        Category::find($id)->update([
            'category_name' => $request->category_name,
            'description' => $request->description,
            'packet_id' => $request->packet_id,
            'file_pdf' => $file_name
        ]);
        
        $this->packet->updateCategoryPacket($request->packet_id);

        return redirect()->route('kategori.index')->with('success', 'Berhasil mengedit data kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $packet = Packet::find($category->packet_id);

        $packet->update(['number_of_categories' => $packet->number_of_categories - 1]);

        unlink('storage/file_pdf/'.$category->file_pdf);
        $category->delete();
        return redirect()->route('kategori.index')->with('success', 'Berhasil menghapus data kategori');
        
    }
}
