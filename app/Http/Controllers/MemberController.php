<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\Packet;
use App\Category;
use App\Video;
use App\Test;
use App\Answer;
use App\Exam;
use App\ExamAnswer;
use App\Notifications\AnswerNotification;
use Auth;
use DB;
use App\Notifications\BuyPacketNotification;
use App\Notifications\SendKodeNotification;
use PDF;

class MemberController extends Controller
{
    private $member;
    private $auth;
    private $packet;
    private $category;
    private $exam;
    private $examAnswer;

    public function __construct(){
        $this->auth = new Auth();
        $this->member = new Member();
        $this->packet = new Packet();
        $this->category = new Category();
        $this->video = new Video();
        $this->exam = new Exam();
        $this->examAnswer = new ExamAnswer();
    }
    public function dashboard() {
        $materi = 0;
        $video = 0;
        $data = DB::table('members_packets')->where('member_id', auth()->user()->id)->get();
        foreach($data as $row){
            $qmateri = Category::where('packet_id', $row->packet_id)->get();
            $materi += $qmateri->count();

            foreach($qmateri as $r){
                $video += $r->videos->count();
            }
        }

        $data['paket'] = $data->count();
        $data['materi'] = $materi;
        $data['video'] = $video;

        return view('member.dashboard', $data);
    }
    
    //show all paket that member have
    public function showPacket(){
        $member_id = $this->auth::guard('member')->user()->id;
        $members = $this->member->findOrFail($member_id);
        $data['packets'] = $members->enrollments()->withPivot(['activation', 'status', 'payment_date'])->get();
        return view('member.packet.index', $data);
    }

    //show detail packet
    public function detailPacket($id){
        $data['packet'] = $this->packet->findOrFail($id);
        $data['categories'] = $this->category->where('packet_id', $id)->get();
        return view('member.packet.detail', $data);
    }

    //show all lesson (kategori) in paket
    public function lessonPacket($id) {
        $data['categories'] = $this->category->where('packet_id', $id)->get();
        $member = Auth::guard('member')->user();
        $ujian = DB::table('members_packets')->where('member_id', $member->id)->where('packet_id', $id)->first();
        $data['status_ujian'] = $ujian->status;
        $data['packet'] = Packet::find($id);
        if(!$data['categories']) {
            return abort(404);
        }
        return view('member.lesson.index', $data);
    }

    //video
    public function watch($packet_id, $category_id, $slug = null){
        if($slug != null && $slug != 'test'){
            $data['content_video'] = $this->video->where('slug', 'like', '%'.$slug.'%')->first();
            $data['comments'] = $data['content_video']->comments->where('reply_id', 0);
        }
        if($slug == 'test') {
            $data['test'] = Test::where('category_id', $category_id)->first();
            if($data['test'] == null){
                return redirect()->back()->with('error', 'Data test belum tersedia');
            }
            $data['slug_now'] = 'test';
            $data['answers'] = Answer::where('member_id', Auth::guard('member')->user()->id)->where('test_id', $data['test']->id)->get();
            
            $data['answer_count'] = Answer::where('member_id', Auth::guard('member')->user()->id)->where('test_id', $data['test']->id)->where('score', '>', 50)->count();
        }
        $data['packet_id']= $packet_id;
        $data['videos'] = $this->video->where('category_id', $category_id)->get();
        if(count($data['videos']) == 0){
            return redirect()->back()->with('error', 'Data video belum tersedia');
        }
        $data['category'] = $this->category->find($category_id);
        $data['slug_now'] = $slug;
        
        return view('member.lesson.watch', $data);
    }

    // aktifasi paket
    public function activatePacket($id, Request $request) {
        $this->validate($request, [
            'access_code' => 'required'
        ]);

        $member = Auth::user();
        $packet = $member->findByEnrollmentByAccessCode($request->access_code);

        if (!$packet) {
            return redirect()->back()->with('error', 'Kode activation tidak valid!');
        }

        $member->enrollments()->updateExistingPivot($id, ['activation' => 1, 'access_code' => $packet->pivot->access_code]);
        
        if ($packet->categories->count() > 0) {

            $member->progresses()->attach($packet->categories()->first()->id);
        }

        return redirect()->back()->with('success', 'Paket anda telah berhasil diactivation');
    }

    //TUTOR PAGE
    public function indexTutor(){
        $tutor = Auth::user();
        $data['members'] = $this->member->all();
    
        //dd($data['members']);
        
        return view('tutor.member.index', $data);
    }

    //detail packet yang diambil member
    public function packetMember($member_id){
        $data['member'] = Member::find($member_id);
        $data['packets'] = $data['member']->enrollments;
            
        return view('tutor.member.paket_member', $data);
    }

    public function progressMember($member_id, $packet_id){
        $data['member'] = Member::find($member_id);
        $data['categories'] = Category::where('packet_id', $packet_id)->get();
        $data['packet_id'] = $packet_id;
        return view('tutor.member.progress', $data);
         
    }


    public function beliPaket(Request $request){
        $this->validate($request, [
            'member_id' => 'required',
            'packet_id' => 'required',
        ]);

        $check = DB::table('members_packets')->where('member_id', $request->member_id)->where('packet_id', $request->packet_id)->first();
        if($check != null){
            return redirect()->back()->with('error', 'Gagal membeli paket karena anda telah memiliki paket tersebut');
        } else {
            $member = $this->member->find($request->member_id);
            $paket = $this->packet->find($request->packet_id);

            $member->enrollments()->attach($request->packet_id, [
                'access_code' => generateAccessCode($request->packet_id),
                'status' => 0,
                'activation' => 0,
            ]);

            //email member
            $member->notify(new BuyPacketNotification($paket, $member));
            
            //notif ke tutor
            $paket->tutor->notifications()->create([
                'title' => 'Member '.$member->name. ' menambahkan paket kursus '.$paket->packet_name,
                'text' => 'Mohon mengecek email anda apakah ada balasan berupa bukti pembayaran dari member tersebut atau tidak, jika ada kirimkan kode akses untuk paket tersebut ke member yang bersangkutan',
                'seen' => 0,
                'date' => now(),
                'url' => '/tutor/member/'.$member->id.'/packet'
            ]);
            return redirect()->back()->with('success', 'Berhasil menambahkan paket, silahkan check email anda untuk mendapatkan data rekening dan melakukan pembayaran');
        }

        dd($request->all());
    }

    public function payment(Request $request){
        $this->validate($request, [
            'file_receipt' => 'required'
        ]);

        $request->file('file_receipt')->store('file_receipt');

        $member = Auth::guard('member')->user();
        $course = $member->findMemberPacket()->updateExistingPivot($request->packet_id, [        
            'payment_date' => date('Y-m-d'),
            'payment_receipt' => $request->file_receipt->hashName()
        ]);
        return redirect()->back()->with('success', 'Bukti transfer telah diupload!');
    }

    public function sendKode(Request $request){     
        $member = $this->member->find($request->member_id);
        $paket = $this->packet->find($request->packet_id);

        $member->notify(new SendKodeNotification($member, $paket));   

        $member->notifications()->create([
            'title' => 'Kode Akses',
            'text' => 'Kode akses untuk paket '. $paket->packet_name . ' telah dikirimkan ke email anda, silahkan cek dan masukan kode akses tersebut',
            'seen' => 0,
            'date' => now(),
            'url' => 'member/paket-saya/'
        ]);

        return redirect()->back()->with('success', 'Kode akses telah berhasil dikirim ke email member');

    }

    public function finishLesson($id) {
        $user = Auth::user();
        
        $packet = Packet::whereHas('members', function($q) use($user) {
            $q->where('members.id', $user->id);
        })->find($id);

        $packet->members()->updateExistingPivot($user->id, ['status' => 1]);

        return redirect()->route('member.lesson', $packet->id)->with('warning', 'Anda sudah dapat mengikuti ujian dari paket '.$packet->name);

    }


    //exam 

    public function exam($packet_id){
        $member = Auth::guard('member')->user();

        $data['packet'] = $this->packet->findOrFail($packet_id);
        $data['exam'] = $this->exam->where('packet_id', $packet_id)->first();
        $data['answers'] = $this->examAnswer->where('member_id', Auth::guard('member')->user()->id)->where('exam_id', $data['exam']->id)->get();
        return view('member.lesson.exam', $data);
    }

    public function examSubmit($packet_id, Request $request){
        $this->validate($request, [
            'file_answer_exam' => 'required',
        ]);
        $request->file('file_answer_exam')->store('file_answer_exam');
        $answer = $this->examAnswer->create([
            'exam_id' => $request->exam_id,
            'member_id' => Auth::guard('member')->user()->id,
            'description' => $request->description,
            'file_answer_exam' => $request->file_answer_exam->hashName(),
            'date' => date('Y-m-d H:i:s'),
        ]);

        $tutor = $answer->exam->packet->tutor;
        
        // $tutor->notify(new AnswerNotification($tutor, $answer));
        $tutor->notifications()->create([
            'title' => 'Jawaban Ujian Baru',
            'text' => 'Anda mendapat jawaban baru dari seorang member pada kategori '.$answer->exam->packet->packet_name,
            'seen' => 0,
            'date' => now(),
            'url' => '/'
        ]);
        
        return redirect()->back()->with('success', 'Berhasil mengsubmit jawaban!');
    }

    public function printMember(){
        $data['members'] = Member::all();
        $pdf = PDF::loadView('tutor.member.print', $data);
        return $pdf->download('data-member.pdf');
    }
}
