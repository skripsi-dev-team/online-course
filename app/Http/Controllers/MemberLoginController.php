<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class MemberLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = 'member/dashboard';

    public function __construct()
    {
        $this->middleware('guest:member')->except('logout');
    }

    public function showLoginForm()
    {
        return view('member.login');
    }

    protected function guard()
    {
        return Auth::guard('member');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/member/login');
    }
}
