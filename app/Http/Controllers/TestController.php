<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use App\Category;
use App\Answer;
use Auth;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_kategori)
    {
        $data['kategori'] = Category::find($id_kategori);
        $data['tests'] = Test::where('category_id', $id_kategori)->first();
        
        return view('tutor.test.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_kategori)
    {
        $data['kategori'] =  Category::find($id_kategori);
        return view('tutor.test.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id_kategori, Request $request)
    {
        $this->validate($request, [
            'file_test' => 'required',
            'description' => 'required',
        ]);

        $store_file = $request->file('file_test')->store('file_test');

        Test::create([
            'file_test' => $request->file_test->hashName(),
            'description' => $request->description,
            'date' => date('Y-m-d H:i:s'),
            'category_id' => $id_kategori
        ]);

        return redirect()->route('video.index', ['id_kategori' => $id_kategori])->with('success', 'Berhasil menambah file test, sekarang silahkan tambah video');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function answer($id)
    {
        $data['answers'] = Answer::where('test_id', $id)->get();
        $data['test'] = Test::find($id);    

        return view('tutor.test.answer', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['test'] = Test::find($id);
        $data['kategori'] = Category::find($data['test']->category_id);
        return view('tutor.test.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $test = Test::find($id);
        $file_name = $test->file_test;
        if($request->hasFile('file_test') == true){
            unlink('storage/file_test/'.$file_name);
            $request->file('file_test')->store('file_test');
            $file_name = $request->file_test->hashName();
        }

        $test->update([
            'description' => $request->description,
            'date' => date('Y-m-d H:i:s'),
            'file_test' => $file_name,
        ]);

        return redirect()->route('kategori.view', ['id_kategori' => $test->category_id])->with('success', 'Berhasil mengedit file test');
    }

}
