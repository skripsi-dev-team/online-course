<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use App\Member;
use App\Packet;
use App\Notifications\MemberRegisterWithPacket;
use Auth;

class RegisterMemberController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/member/dashboard';

    public function __construct() {
        $this->middleware('guest:member')->except('verifyEmail');
    }

    protected function validator(array $data) {
        return Validator::make($data, [
            'email' => ['required', 'string', 'unique:members'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'name' => ['required'],
            'phone'   => ['required'],
            'address' => ['required']
        ]);
    }

    protected function create(array $data) {
        $member = Member::create([
            'email' => $data['email'],
            'password'  => $data['password'],
            'name'  => $data['name'],
            'phone'  => $data['phone'],
            'address'    => $data['address']
        ]);
        
        $member->enrollments()->attach($data['packet_id'], [
                'access_code' => generateAccessCode($data['packet_id']), 
                'status' => 0,
                'activation' => 0
            ]
        );
        return $member;
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = $this->create($request->all());


        $member = Member::find($user->id);
        $packet = Packet::find($request->packet_id);

        //email member
        $member->notify(new MemberRegisterWithPacket($member, $packet));

        //notif tutor yang bersangkutan
        //notif ke tutor
        $packet->tutor->notifications()->create([
            'title' => 'Member '.$member->name. ' mendaftarkan paket kursus '.$packet->packet_name,
            'text' => 'Mohon mengecek email anda apakah ada balasan berupa bukti pembayaran dari member tersebut atau tidak, jika ada kirimkan kode akses untuk paket tersebut ke member yang bersangkutan',
            'seen' => 0,
            'date' => now(),
            'url' => '/tutor/member/'.$member->id.'/packet'
        ]);

        return redirect()->route('member.login')->with('success', 'Registrasi berhasil! Anda sudah bisa login. Silahkan cek email anda dan lakukan pembayaran untuk mendapatkan kode aktivasi paket yang anda pilih.');
    }

    public function showRegisterForm() {
        return view('member.register', ['packets' => \App\Packet::has('categories')->orderBy('packet_name')->get()]);
    }

    protected function guard()
    {
        return Auth::guard('member');
    }
}
