<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Category;
use App\Video;

class ApiController extends Controller
{
    public function getNotification(Request $request) {
        
        $user = Auth::user();

        $notifs = $user->notifications()->where('seen', 0)->orderBy('date')->get();

        return response()->json([
            'status' => 'success',
            'data' => $notifs,
            'count' => count($notifs)
        ]);
    }

    public function refreshNotif() {
        $user = Auth::user();

        $notifs = $user->notifications()->where('seen', 0)->update(['seen' => 1]);

        return response()->json([
            'status' => 'ok',
            'count' => $user->notifications()->where('seen', 0)->count()
        ]);
    }

    public function syncPacketCategory($id, Request $request)
    {
        $packet = \App\Packet::find($id);

        $packet->categories()->delete();

        $packet->categories()->createMany($request->all()['data']);

        return response()->json([
            'status' => 'ok'
        ], 201);
    }

    public function syncCategoryVideo($id, Request $request)
    {
        $category = \App\Category::find($id);

        $category->videos()->delete();

        $category->videos()->createMany($request->all()['data']);

        return response()->json([
            'status' => 'ok'
        ], 201);
    }

    public function switchCategoryOrder($old, $new)
    {
        $old = Category::findOrFail($old);
        $new = Category::findOrFail($new);

        $temp = $old->ordering;

        $old->ordering = $new->ordering;
        $old->save();

        $new->ordering = $temp;
        $new->save();

        return response()->json([
            'status' => 'ok',
        ], 200);
    }

    public function switchCategoryVideoOrder($old, $new)
    {
        $old = Video::findOrFail($old);
        $new = Video::findOrFail($new);

        $temp = $old->ordering;

        $old->ordering = $new->ordering;
        $old->save();

        $new->ordering = $temp;
        $new->save();

        return response()->json([
            'status' => 'ok',
        ], 200);
    }
}
