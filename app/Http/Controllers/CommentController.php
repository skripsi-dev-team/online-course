<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Comment;
use App\Tutor;
use App\Notifications\CommentNotification;
use App\Video;
use App\Member;

class CommentController extends Controller
{
    public function member_create(Request $request){
        $this->validate($request, ['comment' => 'required']);

        $now = now();
        $member_id = Auth::guard('member')->user()->id;
        $comment = Comment::create([
            'comment' => $request->comment,
            'member_id' => $member_id,
            'video_id' => $request->video_id,
            'date' => $now,
        ]);
        $tutor = $comment->video->category->packet->tutor;
        $tutor->notify(new CommentNotification($tutor));
        $tutor->notifications()->create([
            'title' => 'comment Baru',
            'text' => 'Anda mendapat comment baru dari seorang member pada video '.$comment->video->title,
            'seen' => 0,
            'date' => $now
        ]);
        return redirect()->back();
    }
    
    public function showComment($category_id, $id){
        $data['video'] = Video::find($id);
        $data['comments'] = Comment::where('video_id', $id)->where('reply_id', 0)->get();
        return view('tutor.video.comment', $data);
    }

    public function deleteComment($id){
        Comment::find($id)->delete();
        return redirect()->back()->with('success', 'Data comment telah dihapus');
    }

    public function replyComment(Request $request){
        $this->validate($request, ['comment' => 'required']);
        
        $member = Member::find($request->member_id);
        
        $reply = Comment::create([
            'comment' => $request->comment,
            'member_id' => $member->id,
            'video_id' => $request->video_id,
            'date' => date('Y-m-d H:i:s'),
            'reply_id' => $request->reply_id,
            'tutor_id' => Auth::guard('tutor')->user()->id
        ]);

        $member->notifications()->create([
            'title' => 'comment Baru dari '.$reply->tutor->name,
            'text' => 'Anda balasan dari comment pada video '.$reply->video->title,
            'seen' => 0,
            'date' => now()
        ]);
        return redirect()->back()->with('success', 'comment berhasil dibalas');
    }
}
