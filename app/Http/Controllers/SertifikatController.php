<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sertificate;

class SertifikatController extends Controller
{
    public function index()
    {
        $data['sertifikat'] = Sertificate::all();
        return view('tutor.sertifikat.index', $data);
    }

    public function show($certificate)
    {
        $data['certificate'] = Sertificate::where('certificate_number', $certificate)->first();
        if(!$data['certificate']){
            return redirect(404);
        }
        return view('tutor.sertifikat.show', $data);
    }
}
