<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'title',
        'seen',
        'text',
        'date',
        'url'
    ];

    public function notifable() {
        return $this->morphTo();
    }
}
