<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Comment extends Pivot
{
    protected $table = 'comments';
    protected $fillable = [
        'comment',
        'date',
        'tutor_id',
        'member_id',
        'video_id',
        'reply_id'
    ];

    public function video() {
        return $this->belongsTo(Video::class);
    }

    public function member(){
        return $this->belongsTo(Member::class);
    }

    public function replies(){
        return $this->hasMany($this, 'reply_id');
    }

    public function tutor(){
        return $this->belongsTo(Tutor::class);
    }
}
