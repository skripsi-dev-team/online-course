<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use App\Notifications\MemberRegisteredNotification;

class Member extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $fillable = [
        'email',
        'password',
        'name',
        'phone',
        'address',
        'email_verified_at'
    ];

    protected $hidden = ['password'];

    public function setPasswordAttribute($val) {
        return $this->attributes['password'] = bcrypt($val);
    }

    public function comments() {
        return $this->belongsToMany(Tutor::class, 'comments', 'member_id', 'tutor_id');
    }

    public function sertificates() {
        return $this->hasMany(Sertificate::class);
    }

    public function chats() {
        return $this->belongsToMany(Tutor::class, 'chats', 'member_id', 'tutor_id');
    }

    public function progresses() {
        return $this->belongsToMany(Category::class, 'progresses', 'member_id', 'category_id');
    }

    public function enrollments() {
        return $this->belongsToMany(Packet::class, 'members_packets', 'member_id', 'packet_id')->withTimestamps();
    }

    public function hasVerifiedEmail() {
        if ($this->email_verified_at == null) {
            return false;
        }
        return true;
    }

    public function markEmailAsVerified() {
        $verify = $this->email_verified_at = now();
        return $this->save();
    }

    public function sendEmailVerificationNotification() {
        $this->notify(new MemberRegisteredNotification($this));
    }

    public function notifications() {
        return $this->morphMany(Notification::class, 'notifable');
    }

    public function findByEnrollmentByAccessCode($code) {
        return $this->enrollments()->withPivot('access_code')->wherePivot('access_code', $code)->first();
    }

    public function findMemberPacket()
    {
        return $this->enrollments()->withPivot('access_code', 'payment_date', 'payment_receipt');
    }
}
