<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packet extends Model
{
    protected $fillable = [
        'packet_name',
        'packet_price',
        'description',
        'tutor_id',
        'best_selling'
    ];

    public function tutor() {
        return $this->belongsTo(Tutor::class);
    }

    public function updateCategoryPacket($packet_id) {
        $number_of_kategori = Category::where('packet_id', $packet_id)->count();
        return Packet::find($packet_id)->update([
            'number_of_categories' => $number_of_kategori
        ]);
    }

    public function members() {
        return $this->belongsToMany('App\Member', 'members_packets', 'packet_id', 'member_id')->withPivot('payment_date', 'payment_receipt', 'id');
    }

    public function notifications() {
        return $this->morphMany(Notification::class, 'notifable');
    }

    public function categories() {
        return $this->hasMany(Category::class, 'packet_id');
    }

    public function checkBestSelling()
    {
        if($this->best_selling == 0){
            return '<span class="badge badge-danger">False</span>';
        } else {
            return '<span class="badge badge-success">True</span>';
        }
    }
   
}
