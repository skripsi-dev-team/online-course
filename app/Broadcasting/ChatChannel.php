<?php

namespace App\Broadcasting;

use App\Tutor;
use Auth;

class ChatChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param  \App\User  $user
     * @return array|bool
     */
    public function join(Tutor $tutor)
    {
        return ($tutor->id) ? true : false;
    }
}
