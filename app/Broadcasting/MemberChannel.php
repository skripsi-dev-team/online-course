<?php

namespace App\Broadcasting;

use App\Member;

class MemberChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param  \App\User  $user
     * @return array|bool
     */
    public function join(Member $member)
    {
        return ($member->id) ? true : false;
    }
}
