<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticable;
use Illuminate\Notifications\Notifiable;

class Tutor extends Authenticable
{
    use Notifiable;

    protected $fillable = [
        'email',
        'username',
        'password',
        'name',
        'phone',
        'address',
        'role'
    ];

    protected $hidden = ['password'];

    public function setPasswordAttribute($val)
    {
        return $this->attributes['password'] = bcrypt($val);
    }

    public function packets() {
        return $this->hasMany(Packet::class);
    }

    public function categories() {
        return $this->hasManyThrough(Category::class, Packet::class, 'tutor_id', 'packet_id');
    }

    public function comments() {
        return $this->belongsToMany(Member::class, 'comments', 'tutor_id', 'member_id');
    }

    

    public function chats() {
        return $this->belongsToMany(Member::class, 'chats', 'tutor_id', 'member_id');
    }

    public function members() {
        return $this->belongsToMany(Member::class, 'members_packets', 'packet_id', 'member_id');
    }

    public function isSuperAdmin() {
        if ($this->role == 1) {
            return true;
        }
        return false;
    }

    public function notifications() {
        return $this->morphMany(Notification::class, 'notifable');
    }
}
