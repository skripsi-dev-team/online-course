<?php 

use Illuminate\Support\Facades\DB;
use App\Packet;
if (!function_exists('urlHasPrefix')) {
    function urlHasPrefix(string $prefix){
        $url = url()->current();
        if (strpos($url, $prefix) > 0) {
            return true;
        }
    
        return false;
    }
}

if (!function_exists('generateAccessCode')) {
    function generateAccessCode($packetId) {
        return 'PK-'.now()->format('Ymd').$packetId.'-'.(\DB::table('members_packets')->count() + 1);
    }
}

if (!function_exists('resizeImage')){
    function resizeImage($image){
        $img = Image::make('storage/video_thumbnail/'.$image);
        $img->fit(150, 150, function($constraint){
            $constraint->upsize();
        });

        $img->save('storage/video_thumbnail/small/'.$image);
        return $img;
    }
}

if (!function_exists('showCategoryPacket')){
    function showCategoryPacket($packetId){
        return App\Category::where('packet_id', $packetId)->first();
    }
}

if (!function_exists('checkActiveVideo')){
    function checkActiveVideo($slug, $slug_now){
        if($slug == $slug_now){
            return 'active';
        } else {
            return '';
        }
    }
}

if (!function_exists('inProgress')) {
    function inProgress($id) {
        $member = Auth::user();
        $cek = $member->progresses()->wherePivot('category_id', $id)->first();
        
        return ($cek) ? true : false;        
    }
}

if (!function_exists('passed')) {
    function passed($id) {
        $member = Auth::user();
        $check = $member->progresses()->wherePivot('pass', 1)->wherePivot('category_id', $id)->first();

        return ($check) ? true : false;
    }
}

if (!function_exists('getKodeAkses')) {
    function getKodeAkses($packet_id, $member_id) {
        $paket =  DB::table("members_packets")->where('packet_id', $packet_id)->where('member_id', $member_id)->first();
        return $paket->access_code;
    }
}

if (!function_exists('checkMemberProgress')) {
    function checkMemberProgress($member_id, $category_id) {
        $progress = DB::table('progresses')
                    ->where('member_id', $member_id)
                    ->where('category_id', $category_id)
                    ->first();
        if($progress === null){
            return false;
        }
        if($progress->pass == 1){
            return true;
        } else {
            return false;
        }
    }
}


if (!function_exists('checkactivation')) {
    function checkactivation($packet_id, $member_id) {
        $paket =  DB::table("members_packets")->where('packet_id', $packet_id)->where('member_id', $member_id)->first();
        $active = $paket->activation;
        if($active == 0){
            return "Belum Aktif";
        } else {
            return "Sudah Aktif";
        }
    }
}

if (!function_exists('checkSertifikat')) {
    function checkSertifikat($member_id, $packet_id) {
       $status = DB::table('members_packets')->where('packet_id', $packet_id)->where('member_id', $member_id)->first();
        return $status->status;
    }
}

if (!function_exists('getProgress')) {
    function getProgress($packet) {
        $user = Auth::user();
        $progress_total = $packet->categories->count();
        
        $percentage = ($user->progresses()->where('packet_id', $packet->id)->wherePivot('pass', 1)->count() / $progress_total) * 100;

        return number_format($percentage, 1);
    }
}


function getPayment($packet_id) {
    $packet = Packet::find($packet_id)->members()->get();
    // foreach($packet as $test){
    //     dd($test->pivot->id);
    // }
    // dd($packet);

    return $packet;
}