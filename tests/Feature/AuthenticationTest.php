<?php

namespace Tests\Feature;

use App\Member;
use App\Tutor;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationTest extends TestCase
{

    public function testAuthenticateTutor()
    {

        $tutor = Tutor::first();

        $response = $this->post('/tutor/login', [
            'email' => $tutor->email,
            'password' => 'secret',
        ]);

        $response->assertStatus(302)->assertRedirect('/tutor/dashboard');
        $this->assertAuthenticated('tutor');
    }

    public function testRedirectUserToLoginFormIfMemberNotAuthenticated()
    {
        $member = Member::first();

        $response = $this->actingAs($member)->get('/member/dashboard');

        $response->assertStatus(302)->assertRedirect('/member/login');
    }

    public function testRedirectUserToLoginFormIfTutorNotAuthenticated()
    {
        $tutor = Tutor::first();

        $response = $this->actingAs($tutor)->get('/tutor/dashboard');

        $response->assertStatus(302)->assertRedirect('/tutor/login');
    }
}
