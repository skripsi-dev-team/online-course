<?php

namespace Tests\Feature;

use App\Tutor;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CrudTutorTest extends TestCase
{
    public function testListTutor()
    {
        $tutor = Tutor::first();

        $response = $this->actingAs($tutor, 'tutor')->get('/tutor/tutor');

        $response->assertStatus(200)->assertViewIs('tutor.index');
    }

    public function testCreateTutorForm()
    {
        $tutor = Tutor::first();

        $response = $this->actingAs($tutor, 'tutor')->get('/tutor/tutor/create');

        $response->assertStatus(200)->assertViewIs('tutor.create');
    }

    public function testStoreTutor()
    {
        $tutor = factory(Tutor::class)->make();

        $response = $this->actingAs(Tutor::first(), 'tutor')->post('/tutor/tutor', [
            'username' => $tutor->username,
            'email' => $tutor->email,
            'name'  => $tutor->name,
            'password' => $tutor->password,
            'password_confirmation' => $tutor->password,
            'phone' => $tutor->phone,
            'address' => $tutor->address,
            'role' => $tutor->role
        ]);

        $response->assertRedirect('/tutor/tutor')->assertSessionHas('success');
        $this->assertDatabaseHas('tutors', $tutor->toArray());

    }

    public function testEditTutor()
    {
        $tutor = Tutor::first();

        $response = $this->actingAs($tutor, 'tutor')->get('/tutor/tutor/'.$tutor->id.'/edit');

        $response->assertStatus(200)->assertViewIs('tutor.edit');
    }

    public function testUpdateTutor()
    {
        $tutor = Tutor::first();
        $tutor2 = factory(Tutor::class)->create();

        $response = $this->actingAs($tutor, 'tutor')->put('/tutor/tutor/'.$tutor2->id, $tutor2->toArray());

        $response->assertRedirect('/tutor/tutor')->assertSessionHas('success');
        $this->assertDatabaseHas('tutors', $tutor2->toArray());
    }

    public function testDeleteTutor()
    {
        $tutor = Tutor::first();
        $tutor2 = factory(Tutor::class)->create();

        $response = $this->actingAs($tutor2, 'tutor')->delete('/tutor/tutor/'.$tutor->id);

        $response->assertRedirect('/tutor/tutor')->assertSessionHas('success');

        $this->assertDatabaseMissing('tutors', $tutor->toArray());
    }

}
