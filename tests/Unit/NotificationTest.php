<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Member;
use App\Tutor;

class NotificationTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testNotif()
    {
        $member = Member::first();
        $notif = $member->notifications()->create([
            'title' => 'Test notif',
            'text' => 'wawawa',
            'seen' => 0,
            'date' => now()
        ]);

        $this->assertTrue(true);
        $this->assertDatabaseHas('notifications', array_merge($notif->toArray(), ['notifable_id' => $member->id, 'notifable_type' => Member::class]));
    }
}
