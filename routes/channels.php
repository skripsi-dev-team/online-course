<?php

use App\Broadcasting\ChatChannel;
use App\Broadcasting\MemberChannel;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('tutor.{tutor}', ChatChannel::class, [
    'guards' => ['web', 'tutor']
]);

Broadcast::channel('member.{member}', MemberChannel::class, [
    'guards' => ['web', 'member']
]);