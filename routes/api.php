<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('chat-to-tutor/send/{member}/{packet}', 'ChatApiController@sendChat')->name('api.chat.send');
Route::post('chat-to-member/send/{tutor}/{member}', 'ChatApiController@sendChatToMember')->name('api.chat.send.member');


Route::prefix('history')->group(function() {
    Route::get('tutor-history/{tutor}/with/{member}', 'ChatApiController@loadHistoryForTutor')->name('api.tutor.history');
    Route::get('member-history/{member}/with/{tutor}', 'ChatApiController@loadHistoryForMember')->name('api.member.history');
});