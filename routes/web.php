<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //$packets = DB::table('members_packets')->select('packet_id', DB::raw('count(packet_id) as jumlah'))->join('packets', 'packets.id', '=', 'members_packets.packet_id')->groupBy('packet_id')->orderBy('jumlah')->limit(3)->get();
    $packets = App\Packet::has('categories')->orderBy('best_selling', 'desc')->limit(3)->get();
    //dd($packets);
    return view('welcome')->with('packets', $packets);
});


Route::prefix('member')->group(function() {
    
    Route::group(['middleware' => ['auth-member:member'] ], function() {
        Route::get('dashboard', 'MemberController@dashboard')->name('member.dashboard');
        Route::get('paket-saya', 'MemberController@showPacket')->name('member.paket_saya');
        Route::get('paket-saya/{id}/detail', 'MemberController@detailPacket')->name('member.detail_paket')->middleware('packet.activated');
        Route::get('lesson/{id}', 'MemberController@lessonPacket')->name('member.lesson')->middleware('packet.activated');
        Route::put('lessen/{id}/finish', 'MemberController@finishLesson')->name('member.lesson.finish')->middleware('packet.activated');
        Route::get('lesson/{id}/watch/{id_kategori}/', 'MemberController@watch')->name('lesson.watch');
        Route::get('lesson/{id}/watch/{id_kategori}/{slug}', 'MemberController@watch')->name('lesson.video');
        Route::get('lesson/{id}/watch/{id_kategori}/test', 'MemberController@watch')->name('lesson.test');
        Route::post('lesson/{id}/watch/{id_kategori}/test', 'AnswerController@create')->name('lesson.answer');
        Route::post('lesson/addcomment', 'CommentController@member_create')->name('lesson.addcomment');
        Route::put('activation-paket/{id}', 'MemberController@activatePacket')->name('member.packet.activate');
        Route::post('beli-paket', 'MemberController@beliPaket')->name('member.beli');
        Route::post('payment', 'MemberController@payment')->name('member.payment');

        //route exam
        Route::get('lesson/{id}/exam', 'MemberController@exam')->name('member.exam');

        Route::post('lesson/{id}/exam', 'MemberController@examSubmit')->name('member.answer_exam');
    });

    Route::get('register', 'RegisterMemberController@showRegisterForm');
    Route::post('register', 'RegisterMemberController@register')->name('member.register');

    Route::get('login', 'MemberLoginController@showLoginForm')->name('member.login-form');
    Route::post('login', 'MemberLoginController@login')->name('member.login');
    Route::post('logout', 'MemberLoginController@logout')->name('member.logout');

});

Route::get('packets', 'PacketController@indexMember')->name('member.packets');
Route::get('packets/{id}/detail', 'MemberController@detailPacket')->name('member.packets.detail');



Route::prefix('api')->group(function() {
    Route::middleware('auth:tutor')->group(function() {
        Route::get('tutor/notifications', 'ApiController@getNotification');
        Route::get('tutor/refresh-notifications', 'ApiController@refreshNotif');

        Route::get('switch-category/{from}/{to}', 'ApiController@switchCategoryOrder');
        Route::get('switch-video/{from}/{to}', 'ApiController@switchCategoryVideoOrder');

        Route::put('tutor/{id}/syncpacketcategory', 'ApiController@syncPacketCategory')->name('api.sync.paket.category');
        Route::put('category/{id}/synccategoryvideo', 'ApiController@syncCategoryVideo')->name('api.sync.category.video');
    });

    Route::middleware('auth:member')->group(function() {
        Route::get('member/notifications', 'ApiController@getNotification');
        Route::get('member/refresh-notifications', 'ApiController@refreshNotif');
    });
});

Route::prefix('tutor')->group(function() {

    Route::get('login', 'TutorLoginController@showLoginForm')->name('tutor.login-form');
    Route::post('login', 'TutorLoginController@login')->name('tutor.login');
    Route::post('logout', 'TutorLoginController@logout')->name('tutor.logout');

    Route::middleware('auth-tutor:tutor')->group(function() {

        Route::get('dashboard', function() {
            $member = \App\Member::all();
            $packet = \App\Packet::all();
            $category = \App\Category::all();
            $data['member'] = $member->count();
            $data['packet'] = $packet->count();
            $data['category'] = $category->count();
            return view('tutor.dashboard', $data);
        })->name('tutor.dashboard');

        Route::resource('paket', 'PacketController');
        Route::resource('kategori', 'CategoryController');
        Route::resource('tutor', 'TutorController');

        Route::get('kategori/{id_kategori}/view', 'CategoryController@view')->name('kategori.view');
        
        //exam route
        Route::get('exam/{id}', 'ExamController@index')->name('exam.index');
        Route::get('exam/{id}/create', 'ExamController@create')->name('exam.create');
        Route::post('exam/{id}/store', 'ExamController@store')->name('exam.store');
        Route::get('exam/{id}/edit', 'ExamController@edit')->name('exam.edit');
        Route::put('exam/{id}/update', 'ExamController@update')->name('exam.update');

        Route::get('exam/{id}/jawaban_ujian_member', 'ExamController@answerExam')->name('exam.answer');
        Route::put('exam/set_nilai_ujian', 'ExamController@setNilaiExam')->name('exam.answer_test');

        Route::get('member', 'MemberController@indexTutor')->name('tutor.member');
        Route::get('member/{id}/packet', 'MemberController@packetMember')->name('tutor.member.packet');  
        Route::get('member/{id}/packet/{id_paket}/progress', 'MemberController@progressMember')->name('tutor.member.progress');

        //route add paket in category page
        Route::post('kategori/addpaket', 'CategoryController@storePacket')->name('kategori.addpaket');
        Route::get('kategori/{id}/video', 'VideoController@index')->name('video.index');
        Route::get('kategori/{id}/video/create', 'VideoController@create')->name('video.create');
        Route::post('kategori/{id}/video/create', 'VideoController@store')->name('video.store');
        Route::get('kategori/{id_kategori}/video/{id}', 'VideoController@show')->name('video.show');
        Route::get('kategori/{id_kategori}/video/edit/{id}', 'VideoController@edit')->name('video.edit');
        Route::put('kategori/{id_kategori}/video/edit/{id}', 'VideoController@update')->name('video.update');
        Route::DELETE('kategori/{id_kategori}/video/{id}', 'VideoController@destroy')->name('video.destroy');
        Route::get('kategori/{id_kategori}/video/{id}/comment', 'CommentController@showComment')->name('comment.show');
        Route::DELETE('comment/{id}', 'CommentController@deleteComment')->name('comment.delete');
        Route::post('comment/reply', 'CommentController@replyComment')->name('comment.reply');

        Route::get('kategori/{id_kategori}/test', 'TestController@index')->name('kategori.test');
        Route::get('kategori/{id_kategori}/test/create', 'TestController@create')->name('test.create');
        Route::post('kategori/{id_kategori}/test/create', 'TestController@store')->name('test.store');
        Route::get('kategori/test/{id}/edit', 'TestController@edit')->name('test.edit');
        Route::put('kategori/test/{id}/edit', 'TestController@update')->name('test.update');
        Route::get('kategori/test/{id}/answer', 'TestController@answer')->name('test.answer');
        Route::put('kategori/test/answer/', 'AnswerController@setNilai')->name('answer.nilai');

        Route::put('send-sertifikat/{member_id}/{packet_id}', 'AnswerController@sendSertifikat')->name('send_sertifikat');

        Route::post('send-kode', 'MemberController@sendKode')->name('tutor.send-kode');

        Route::get('print-member', 'MemberController@printMember')->name('print.member');

        //nilai test
        Route::get('nilai-test', 'AnswerController@nilaiTest')->name('tutor.nilai.test');
        Route::get('nilai-test/print', 'AnswerController@printNilai')->name('nilai.test.print');

        //nilai ujian
        Route::get('nilai-ujian', 'ExamController@nilaiUjian')->name('tutor.nilai.ujian');
        
        Route::get('nilai-ujian/print', 'ExamController@printUjian')->name('nilai.ujian.print');


        Route::get('payment', 'PacketController@payment')->name('tutor.payment');

        Route::put('payment/{id}', 'PacketController@deletePayment')->name('tutor.payment.delete');

        Route::get('sertifikat', 'SertifikatController@index')->name('tutor.sertifikat');

      

        
        
    });
});
Route::get('generate-sertifikat/member/{id_member}/paket/{id_paket}/sertifikat/{certificate_number}', 'AnswerController@generateSertifikat')->name('tutor.generate-sertifikat');

Route::get('sertifikat/show/{kode}', 'SertifikatController@show')->name('sertifikat.show');

Route::get('verification-failed', function() {
    return view('verification-notice');
})->name('verification.notice');

Route::get('tutor', function(){
    return redirect()->route('tutor.dashboard');
});

Route::get('member', function(){
    return redirect()->back();
});

Route::get('sertifikat', function(){
    return view('tutor.sertifikat.page');
}); 

