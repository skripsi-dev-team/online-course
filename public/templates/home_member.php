<?php 
require_once "header.php";
?>

<div class="container">
    <div class="row mt-5 mb-3">
        <div class="col-md-12">
            <h2>Selamat Datang</h2>
            <hr>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi quae officiis deserunt itaque repellat doloribus accusamus cumque voluptatibus perspiciatis, architecto dolorem obcaecati necessitatibus? Eligendi impedit a deserunt! Cupiditate, fugit similique?</p>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <h4 class="">Paket Kursus Yang Anda Miliki</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 mt-3">
            <div class="card shadow mb-4 card-member">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Nama Paket</h6>
                </div>
                <div class="card-body">
                  <p>The styling for this basic card example is created by using default Bootstrap utility classes. By using utility classes, the style of the card component can be easily modified with no need for any custom CSS!</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                    </ul>
                    <div class="text-center">
                        <a href="#" class="btn btn-outline-success btn-block">Start!</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-3">
            <div class="card shadow mb-4 card-member">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Nama Paket</h6>
                </div>
                <div class="card-body">
                  <p>The styling for this basic card example is created by using default Bootstrap utility classes. By using utility classes, the style of the card component can be easily modified with no need for any custom CSS!</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                    </ul>
                    <div class="text-center">
                        <a href="#" class="btn btn-outline-success btn-block">Start!</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 mt-3">
            <div class="card shadow mb-4 card-member">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Nama Paket</h6>
                </div>
                <div class="card-body">
                  <p>The styling for this basic card example is created by using default Bootstrap utility classes. By using utility classes, the style of the card component can be easily modified with no need for any custom CSS!</p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                        <li>Lorem ipsum dolor sit amet</li>
                    </ul>
                    <div class="text-center">
                        <a href="#" class="btn btn-outline-success btn-block">Start!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
require_once "footer.php";
?>