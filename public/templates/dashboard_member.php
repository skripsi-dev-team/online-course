<?php 
require_once "header.php";
?>

    <div class="container-fluid">
        <div class="row mb-3 row-content-video">
            <div class="col-md-9 col-video">

                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/7Ji3eD3h09k" allowfullscreen></iframe>
                </div>
            </div>

            <div class="col-md-3 col-sidebar">
                <div class="sidebar-video">
                    <ul class="list-video">
                        <li class="list-video-title">Nama Kategori Course</li>
                        <li class="watched"><a href="">Tutorial Lorem ipsum dolor sit amet</a></li>
                        <li class="active"><a href="">Tutorial Lorem ipsum dolor sit amet</a></li>
                        <li><a href="">Tutorial Lorem ipsum dolor sit amet</a></li>
                        <li><a href="">Tutorial Lorem ipsum dolor sit amet</a></li>
                        <li><a href="">Tutorial Lorem ipsum dolor sit amet</a></li>
                        <li><a href="">Tutorial Lorem ipsum dolor sit amet</a></li>
                        <li><a href="">Tutorial Lorem ipsum dolor sit amet</a></li>
                        <li><a href="">Tutorial Lorem ipsum dolor sit amet</a></li>
                        <li><a href="">Tutorial Lorem ipsum dolor sit amet</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row mt-4">
            <div class="container">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header text-primary">
                            <h4>description</h4>
                        </div>
                        <div class="card-body">
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis illo repellendus unde vitae hic! Pariatur repellendus debitis odio numquam dignissimos, eveniet quaerat mollitia sint reiciendis nulla dolorem temporibus itaque deserunt!</p>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis illo repellendus unde vitae hic! Pariatur repellendus debitis odio numquam dignissimos, eveniet quaerat mollitia sint reiciendis nulla dolorem temporibus itaque deserunt!</p>
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis illo repellendus unde vitae hic! Pariatur repellendus debitis odio numquam dignissimos, eveniet quaerat mollitia sint reiciendis nulla dolorem temporibus itaque deserunt!</p>
                        
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>

        <div class="row mt-4">
            <div class="container">
             
                    <div class="col-md-12">
                        <h4 class="commentar-title mb-4 text-primary">comment</h4>
                        <div class="card">
                            <div class="card-body">
                                <ul class="list-comment">
                                    <li>
                                        <div class="username">
                                            Nama User
                                        </div>
                                        <div class="comment">
                                            Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                                        </div>
                                    </li>
                                    <li class="reply">
                                        <div class="username">
                                            Nama User
                                            
                                        </div>
                                        <small><i>09-09-2019 20:00:00</i></small>
                                        <div class="comment mt-3">
                                            Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                                        </div>
                                    </li>
                                    <li>
                                        <div class="username">
                                            Nama User
                                        </div>
                                        <div class="comment">
                                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquam, doloribus illum. Iure blanditiis vel, modi inventore tenetur tempore architecto nulla distinctio nam, exercitationem eum iste nobis voluptatum odit soluta! Quisquam.
                                        </div>
                                    </li>
                                    <li>
                                        <div class="username">
                                            Nama User
                                        </div>
                                        <div class="comment">
                                            Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                                        </div>
                                    </li>
                                </ul>

                                <h5>Tambah comment</h5>
                                <form>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="4" placeholder="Masukan comment disini"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-lg btn-success" value="Simpan">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                        
        </div>
        
    </div>

<?php 
require_once "footer.php";
?>