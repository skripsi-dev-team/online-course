<?php 
require_once "header.php";
?>

<div class="container">
    <div class="row mt-5 mb-3">
        <div class="col-md-12">
            <h2>Selamat Datang</h2>
            <hr>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi quae officiis deserunt itaque repellat doloribus accusamus cumque voluptatibus perspiciatis, architecto dolorem obcaecati necessitatibus? Eligendi impedit a deserunt! Cupiditate, fugit similique?</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card shadow mb-4 card-member">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold">Kursus saya</h6>
                </div>
                <div class="card-body">
                    <p>Belajar Akuntansi Dasar</p>
                    <img src="img.jpg" alt="te" class="img-fluid">
                    <div class="progress mt-3">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
                    </div>
                    <div class="text-center mt-3">
                        <a href="#" class="btn btn-outline-primary btn-block">Lanjutkan Kursus!</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h4>Kategori Kursus: </h4>
            <hr>
            <a href="" class="member-category watched">
                <div class="card border-left-primary">
                    <div class="card-body">
                        Lorem ipsum dolor sit amet
                    </div>
                </div>
            </a>
            <a href="" class="member-category">
                <div class="card border-left-primary">
                    <div class="card-body">
                        Lorem ipsum dolor sit amet
                    </div>
                </div>
            </a>
            <a href="" class="member-category disabled">
                <div class="card border-left-primary">
                    <div class="card-body">
                        Lorem ipsum dolor sit amet
                    </div>
                </div>
            </a>
            <a href="" class="member-category">
                <div class="card border-left-primary">
                    <div class="card-body">
                        Lorem ipsum dolor sit amet
                    </div>
                </div>
            </a>
        </div>
    </div>
    
</div>

<?php 
require_once "footer.php";
?>