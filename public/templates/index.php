<?php 
require_once "header.php";
?>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="text-center">Paket 1</h4>
                        <hr>
                        <ul>
                            <li>Belajar dasar dasar akutansi keuangan.</li>
                            <li>Menyusun laporan keuangan secara manual (dengan Exel & Number)</li>
                            <li>Menyusun laporan keuangan dengan sistem software (myob Premier - Accounting , Zero , Vend)</li>
                        </ul>
                        
                        <div class="text-center">
                            <h2>Rp. 1.000.000</h2>
                        </div>
                        <hr>
                        <div class="text-center">
                            <a href="#" class="btn btn-outline-success">Daftar Sekarang</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="text-center">Paket 2</h4>
                        <hr>
                        <ul>
                            <li>Belajar dasar dasar akutansi keuangan</li>
                            <li>Menyusun laporan keuangan secara manual (dengan Exel & Number)</li>
                            <li>Menyusun laporan keuangan dengan sistem software ( myob Premier - Accounting , Zero , Vend)</li>
                            <li>Menyusun laporan keuangan dengan study kasus perusahaan</li>
                            <li>Belajar pajak / tax</li>
                            <li>Aplikasi e-SPT PPH Badan</li>
                            <li>aplikaksi e- SPT OP</li>
                            <li>aplikasi e-SPT PPH pasal 21 karyawan</li>
                            <li>aplikaksi e- Faktur PPn</li>
                            <li>Konsultasi kasus accounting report semua jenis perusahaan (manufacture, dagang, retail, jasa, kontraktor, villa, restoran)</li>
                        </ul>
                        <div class="text-center">
                            <h2>Rp. 1.000.000</h2>
                        </div>
                        <hr>
                        <div class="text-center">
                            <a href="#" class="btn btn-outline-success">Daftar Sekarang</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="text-center">Paket 3</h4>
                        <hr>
                        <ul>
                            <li>Belajar Microsoft Exel, Word dan Power Point dengan windows dan mac</li>
                        </ul>
                        <div class="text-center">
                            <h2>Rp. 850.000</h2>
                        </div>
                        <hr>
                        <div class="text-center">
                            <a href="#" class="btn btn-outline-success">Daftar Sekarang</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="row mt-5">
            <div class="col-md-12">
                <h3>About Us</h3>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis accusantium quisquam velit laboriosam suscipit quibusdam sint iusto repellat illo ad nam similique, voluptatem iste est perferendis soluta nobis architecto id.</p>
            </div>
        </div>
    </div>

 
    
<?php 
require_once "footer.php";
?>