@extends('member.app')

@section('title')
Tonton Video
@endsection

@section('content')

<div class="container-fluid content-video">
    <div class="row mb-3 row-content-video">
        <div class="col-md-9 col-video">
        @include('member.partials.alert')
            @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>                        
            </div>
            @endif

            @if(isset($content_video))
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="{{ $content_video->embed_link }}" allowfullscreen></iframe>
            </div>
            @elseif($slug_now != 'test')
            <div class="card shadow">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Ayo kita mulai!</h6>
                </div>
                <div class="card-body">
                    <h3>{{ strtoupper($category->category_name) }}</h3>
                    <p>{!! $category->description !!}</p>
                    <p>Downlaod Materi PDF: <a href="{{ asset('storage/file_pdf/'.$category->file_pdf) }}">Download</a></p>
                    <p>Klik konten video disamping untuk memulai pembelajaran</p>
                </div>
            </div>
            @else

            <!-- FILE TEST -->
            <div class="card shadow">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Terima kasih telah melihat semua video pada seri ini!</h6>
                </div>
                <div class="card-body">                  

                    {!! $test->description !!}
                    <p>Untuk mengetahui seberapa dalam pemahaman anda tentang materi tersebut, silahkan download dan kerjakan soal test dibawah</p>
                    <p><a href="{{ asset('storage/file_test/'.$test->file_test) }}">Download soal test</a></p>
                    <p>Perlu diketahui bahwa pemahaman anda pada materi ini sangat penting untuk melangkah ke materi selanjutnya, jika anda belum mampu memahami materi ini sangat disarankan anda mempelajari kembali tentang materi tersebut. </p>
                    <p>Jika anda sudah menjawab klik <a href="" data-toggle="modal" data-target="#formUploadModal" class="answer_link">disini</a> untuk mengupload jawaban anda.</p>
                    <p>Mohon menunggu sampai tutor memeriksa jawaban anda dan memberikan anda akses ke materi selanjutnya</p>
                    @if ($test->category->isLatestCategory() && count($answers) > 0)
                        <p><b>Ini adalah kategori terakhir dalam paket ini, apabila jawaban anda sudah dinilai oleh tutor anda dapat melakukan ujian kursus untuk paket ini dengan mengklik tombol dibawah</b></p>
                        <p>
                            <form action="{{ route('member.lesson.finish', $test->category->packet->id) }}" method="POST">
                                @method('PUT')
                                @csrf
                                <button {{ $answer_count === 0 ? 'disabled' : '' }} class="btn btn-primary btn-sm">Aktifkan Ujian Paket Ini</button>
                            </form>
                        </p>
                    @endif
                    <hr>
                    <br><br><br>
                    @if(count($answers) != 0)
                        <h5>Jawaban anda</h5>
                        <table class="table">
                            <tr>
                                <td>No</td>
                                <td>Tanggal</td>
                                <td>File Answer</td>
                                <td>Deskripsi</td>
                                <td>Nilai</td>
                                <td>Review Tutor</td>
                                <td>Ubah</td>
                            </tr>
                            @foreach($answers as $jawaban)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $jawaban->date }}</td>
                                <td><a href="{{ asset('storage/file_answer/'. $jawaban->file_answer) }}">Download</a></td>
                                <td>{{ $jawaban->description }}</td>
                                <td>{{ $jawaban->score ? $jawaban->score : 'Belum ada nilai' }}</td>
                                <td>{{ $jawaban->tutor_description }}</td>
                                <td><a href="" class="btn btn-sm btn-warning editBtn" data-toggle="modal" data-target="#formUploadModal" data-answer="{{ $jawaban->id }}" data-description="{{ $jawaban->description }}"><i class="fa fa-cog"></i></a></td>
                            </tr>
                            @endforeach
                        </table>
                    @endif
                </div>
            </div>
            <!-- Form upload Modal-->
            <div class="modal fade formUploadModal" id="formUploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Upload jawaban anda</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        </div>
                        <div class="modal-body">
                            <p>Jika file lebih dari satu silahkan jadikan file zip/rar terlebih dahulu</p>
                            <form action="{{ route('lesson.answer', ['id' => $packet_id, 'category_id' => $category->id]) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="test_id" value="{{ $test->id }}">
                                <input type="hidden" name="jawaban_id" class="jawaban_id">
                                <input type="file" name="file_answer" class="form-control"><br>
                                <p>Deskripsi singkat mengenai jawaban</p>
                                <textarea name="description" class="form-control deskripsi"></textarea>
                                <br>
                                <div class="float-right">
                                    <input type="submit" value="Simpan" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                       
                    </div>
                </div>
            </div>
            @endif
        </div>

        <div class="col-md-3 col-sidebar">
            <div class="sidebar-video">
                <ul class="list-video">
                    <li class="list-video-title">{{ ucwords($category->category_name) }}</li>                    
                    @foreach($videos as $video)
                        <li class="{{ checkActiveVideo($video->slug, $slug_now) }}"><a href="{{ route('lesson.video', ['packet_id' => $packet_id, 'category_id' => $video->category_id, 'slug' => $video->slug ]) }}">{{ ucwords($video->title) }}</a></li>
                    @endforeach
                    <li class="{{ checkActiveVideo('test', $slug_now) }}"> <a href="{{ route('lesson.test', ['packet_id' => $packet_id, 'category_id' => $video->category_id]) }}">Soal Test</a></li>
                </ul>
            </div>
        </div>
    </div>
    @if(isset($content_video))
    <div class="row mt-4 mb-5">
        <div class="container">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header text-primary">
                        <h4>Deskripsi</h4>
                    </div>
                    <div class="card-body">
                        {!! $content_video->description !!}
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <!-- comment -->
    <div class="row mt-4">
        <div class="container">
            <div class="col-md-12">
                
                <div class="card shadow">
                    <div class="card-header">
                        <h4 class="commentar-title text-primary">Komentar</h4>
                    </div>
                    <div class="card-body">
                        <ul class="list-comment">
                        
                           @if(count($comments) == 0)
                            <li>Tidak ada komentar</li>
                           @else
                           @foreach($comments as $comment)
                           <li>
                                <div class="username">
                                   {{ $comment->member->name }}
                                </div>
                                <small><i>{{ $comment->date }}</i></small>
                                <div class="comment mt-3">
                                    {{ $comment->comment }}                                    
                                </div>
                            </li>
                            @if($comment->replies)
                                @foreach($comment->replies as $reply)
                                <li class="reply">
                                    <div class="username">
                                    {{ $reply->tutor->name }} <small>(Tutor)</small>
                                    </div>
                                    <small><i>{{ $comment->date }}</i></small>
                                    <div class="comment mt-3">
                                        {{ $reply->comment }}
                                    </div>
                                </li>
                                @endforeach
                            @endif
                            @endforeach
                            
                           @endif
                            
                           
                            
                        </ul>

                        <h5>Tambah Komentar</h5>
                        <form action="{{ route('lesson.addcomment') }}" method="post">
                            @csrf
                            <input type="hidden" name="video_id" value="{{ $content_video->id }}">
                            <div class="form-group">
                                <textarea class="form-control" rows="4" placeholder="Masukan komentar disini" name="comment" required></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-lg btn-success" value="Simpan">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>      
    </div>
    @endif
</div>

@endsection


@push('scripts')

<script>

$(document).ready(function(){
    $('.editBtn').click(function(){
        jawaban_id = $(this).data('answer');
        $('.jawaban_id').val(jawaban_id);
        $('.deskripsi').val($(this).data('description'));
    })

    $('.answer_link').click(function(){
        $('.jawaban_id').val('');
        $('.deskripsi').val('');
    })
});

</script>

@endpush