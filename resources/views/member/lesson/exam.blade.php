@extends('member.app')

@section('title')
Ujian {{ $packet->packet_name }}
@endsection

@section('content')

    <div class="container">
    <div class="col-md-12 mb-4">
            
        @include('member.partials.alert')
        @if($errors->any())
        <div class="alert alert-danger">
            <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>                        
        </div>
        @endif

        <h4>Mulai Ujian</h4>
        <br>
        <div class="card border-left-primary">
            <div class="card-body">
                <div class="card-title">
                    <h5>Silahkan Download dan kerjakan soal dibawah</h5>
                    <hr>
                </div>
                {!! $exam->description !!}
                <p>Silahkan <a href="{{ asset('storage/file_exam/'.$exam->file_exam) }}"> Download Soal Ujian Disini</p>
                <hr>
                <p><a href="#" data-toggle="modal" data-target="#formUploadModal" class="btn btn-sm btn-outline-primary">Upload Jawaban</a></p>

                <table class="table">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>File Jawaban</th>
                        <th>Deskripsi</th>
                        <th>Nilai</th>
                        <th>Review Tutor</th>                        
                    </tr>
                    @forelse($answers as $jawaban)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $jawaban->date }}</td>
                        <td><a href="{{ asset('storage/file_answer_exam/'. $jawaban->file_answer_exam) }}">Download</a></td>
                        <td>{{ $jawaban->description }}</td>
                        <td>{{ $jawaban->score ? $jawaban->score : 'Belum ada nilai' }}</td>
                        <td>{{ $jawaban->tutor_description }}</td>
                    </tr>
                    @empty

                    <tr>
                        <td colspan="6" class="text-center">Belum ada jawaban</td>
                    </tr>

                    @endforelse
                </table>
            </div>
        </div>
           
      
    </div>
</div>


<!-- Form upload Modal-->
<div class="modal fade formUploadModal" id="formUploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload jawaban anda</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">
                <p>Jika file lebih dari satu silahkan jadikan file zip/rar terlebih dahulu</p>
                <form action="{{ route('member.answer_exam', ['packet_id' => $packet->id, 'exam_id' => $exam->id]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="exam_id" value="{{ $exam->id }}">
                    <input type="file" name="file_answer_exam" class="form-control"><br>
                    <p>Deskripsi singkat mengenai jawaban</p>
                    <textarea name="description" class="form-control"></textarea>
                    <br>
                    <div class="float-right">
                        <input type="submit" value="Simpan" class="btn btn-success">
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>

@endsection