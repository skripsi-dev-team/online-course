@extends('member.app')

@section('title')
Pembelajaran 
@endsection

@section('content')

    <div class="container">
    <div class="col-md-12 mb-4">
        @include('tutor.flash-message')
            
        @include('member.partials.alert')
        @if($errors->any())
        <div class="alert alert-danger">
            <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>                        
        </div>
        @endif

        <h4>Kategori Pembelajaran</h4>
        <hr>
        <a href="{{ route('member.exam', ['id' => $packet->id]) }}" class="btn btn-sm btn-outline-warning {{ ($status_ujian == 0) ? 'disabled' : '' }}" onclick="return confirm('Sebelum ujian, pastikan anda telah menyelesaikan semua kategori pembelajaran pada paket ini')">Mulai Ujian Untuk Paket Ini</a>
        <br><br>
            @foreach($categories as $data)
                <a href="{{ (!inProgress($data->id)) ? '#' : route('lesson.watch', ['packet_id' => $packet->id, 'category_id' => $data->id]) }}" class="member-category {{ (!inProgress($data->id) && !passed($data->id)) ? 'disabled' : '' }}">
                    <div class="card border-left-primary">
                        <div class="card-body">
                            {{ $data->category_name }}
                        </div>
                    </div>
                </a>
            @endforeach
           
      
    </div>
</div>
<!-- <chat-box 
    channel="{{ 'member.'.Auth::user()->id }}" 
    event="TutorChatSended" 
    api-url="{{ route('api.chat.send', ['member' => Auth::user()->id, 'packet' => $packet->id]) }}"
    history-url="{{ route('api.member.history', ['member' => Auth::user()->id, 'tutor' => $packet->tutor->id]) }}"
/> -->
@endsection