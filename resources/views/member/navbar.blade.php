 <!-- Topbar -->
<nav class="navbar navbar-expand navbar-light topbar mb-4 static-top shadow member-navbar">

    <a class="navbar-brand text-white" href="/">
        <img src="{{ asset('image/logo.png') }}" alt="" width="150">
    </a>
    <!-- Sidebar Toggle (Topbar) -->
    <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse collapse-member" id="navbarSupportedContent">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="{{ route('member.dashboard') }}" class="nav-link">Home</a>
            </li>

            <li class="nav-item">
                <a href="{{ route('member.packets') }}" class="nav-link">Paket</a>
            </li>
        </ul>

        @guest('member')
        <ul class="navbar-nav ml-auto">
            <li class="nav-item mr-2">
                <a href="/member/register" class="btn btn-outline-light">Daftar</a>
            </li>
            <li class="nav-item">
                <a href="/member/login" class="btn btn-light">Login</a>
            </li>
        </ul>
        @else

    
        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
                <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-bell fa-fw"></i>
                    <!-- Counter - Alerts -->
                    <span class="badge badge-danger badge-counter">3+</span>
                </a>
                <!-- Dropdown - Alerts -->
                <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                    <h6 class="dropdown-header">
                    Alerts Center
                    </h6>
                    <a class="dropdown-item d-flex align-items-center" href="#">
                    <div class="mr-3">
                        <div class="icon-circle bg-primary">
                        <i class="fas fa-file-alt text-white"></i>
                        </div>
                    </div>
                    <div>
                        <div class="small text-gray-500">December 12, 2019</div>
                        <span class="font-weight-bold">A new monthly report is ready to download!</span>
                    </div>
                    </a>
                    
                    <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                </div>
            </li>


            <div class="topbar-divider d-none d-sm-block"></div>

        
            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="mr-2 d-lg-inline text-white small">{{ ucwords(Auth::guard('member')->user()->name) }}</span>
                    <!-- <img class="img-profile rounded-circle" src="/60x60"> -->
                </a>
                <!-- Dropdown - User Information -->
                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a href="{{ route('member.paket_saya') }}" class="dropdown-item" href="#">
                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                        Paket Kursus Saya
                    </a>
                    
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                        Logout
                    </a>
                </div>
            </li>
            @endguest

        </ul>
    </div>

</nav>
<!-- End of Topbar -->