@extends('tutor.app')

@section('title')
Login Member
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">

    <div class="col-xl-10 col-lg-12 col-md-9">

      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-6 d-none d-lg-block bg-login-member"></div>
            <div class="col-lg-6">

              <div class="p-5">
                  @include('member.partials.alert')
                  @if ($errors->has('email'))
                      <div class="alert alert-danger">
                        {{ $errors->first('email') }}
                      </div>
                  @endif
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4">TAW Course Member Login</h1>
                </div>
                <form action="{{ route('member.login') }}" method="POST" class="user">
                  @csrf
                  <div class="form-group">
                    <input type="email" class="form-control form-control-user" name="email" value="{{ old('email') }}" placeholder="Masukan Email..">
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control form-control-user" name="password" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <div class="custom-control custom-checkbox small">
                      <input type="checkbox" class="custom-control-input" name="remember" id="customCheck">
                      <label class="custom-control-label" for="customCheck">Remember Me</label>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Login</button>
                </form>
                <hr>
                
                <div class="text-center">
                  <a class="small" href="{{ route('member.register') }}">Create an Account!</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
</div>
@endsection