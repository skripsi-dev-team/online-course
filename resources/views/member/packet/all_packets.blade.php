@extends('member.app')

@section('title')
Paket
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Daftar Paket</h3>
        </div>
        <div class="col-md-12">
            @include('member.partials.alert')
        </div>
    </div>
    <div class="row">
        @foreach($packets as $paket)
        <div class="col-md-4 mb-4 mt-4">
            <div class="card">
                <div class="card-body">
                    <div class="card-text">
                        @if($paket->best_selling == 1)
                        <span class="badge-best">Best Seller</span>
                        @endif
                        <h4 class="text-center">{{ $paket->packet_name }}</h4>
                        <hr>
                        @if(strlen($paket->description) > 300)
                            {!! substr($paket->description, 0, 300) !!}
                        @else 
                            {!! $paket->description !!}
                        @endif
                    </div>
                    <div class="text-center">
                        <h2>Rp. {{ number_format($paket->packet_price) }}</h2>
                    </div>
                    <hr>
                    <div class="text-center">
                        <p>
                            Tutor : <strong>{{ $paket->tutor->name }}</strong>
                        </p>
                        @guest('member')
                        <a href="/member/register?paket={{ $paket->id }}" class="btn btn-success">Daftar Sekarang</a>
                        @else
                        <a href="#" data-toggle="modal" data-target="#beliModal" data-packet="{{ $paket->id }}" data-member="{{ Auth::guard('member')->user()->id }}" class="btn btn-success">Beli Sekarang</a>
                        @endguest
                        <a href="{{ route('member.packets.detail', ['id' => $paket->id]) }}" class="btn btn-outline-info">Detail</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>


@if(Auth::guard('member')->check())
<!-- Beli Modal-->
<div class="modal fade" id="beliModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Yakin akan membeli paket ini?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">Dengan menekan tombol 'Beli Sekarang' berarti anda sudah siap untuk melakukan pembayaran</div>
            <div class="modal-footer">
                <form action="{{ route('member.beli') }}" method="post">
                    @csrf
                    <input type="hidden" value="" name="packet_id" class="packet">
                    <input type="hidden" value="" name="member_id" class="member">
                    <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Ya, beli sekarang</a>
                </form>
            </div>
        </div>
    </div>
</div>

@endif

@endsection


@push('scripts')

<script>

$('#beliModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget); // Button that triggered the modal
  var packet = button.data('packet'); // Extract info from data-* attributes
  var member = button.data('member');
  $('.packet').val(packet);
  $('.member').val(member);
});

</script>

@endpush
