@extends('member.app')

@section('title')
Paket Saya
@endsection

@section('content')
<div class="container">
    <div class="col-md-12 mb-4">
        @include('member.partials.alert')
        <p>Paket yang saya miliki</p>
        <a href="{{ route('member.packets') }}" class="btn btn-sm btn-success mb-3">Beli Paket Lain</a>
        @foreach($packets as $paket)
        
        <div class="card border-left-primary shadow h-100 py-2 mb-4">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold {{ $paket->pivot->status == 1 ? 'text-success' : 'text-primary' }} text-uppercase mb-1">{{ $paket->packet_name }}</div>
                        <div class="row no-gutters align-items-center">
                            <div class="col-auto">
                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ getProgress($paket).'%' }}</div>
                            </div>
                            <div class="col">
                                <div class="progress progress-sm mr-2">
                                <div class="progress-bar {{ $paket->pivot->status == 1 ? 'bg-success' : 'bg-primary' }}" role="progressbar" style="width: {{ getProgress($paket).'%' }}" aria-valuenow="{{ getProgress($paket) }}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                </div>
                @if ($paket->pivot->activation == 0)
                    <div class="row mt-2 mr-1 not-active-row">
                        <div class="col-md-6">
                            
                            @if($paket->pivot->payment_date == null || $paket->pivot->payment_date == '')
                            <a href="#" class="btn btn-sm btn-primary paymentBtn" data-toggle="modal" data-target="#paymentModal" data-paket="{{ $paket->id }}" data-name="{{ $paket->packet_name }}">Upload bukti pembayaran</a>
                            @else 
                            <a href="#" class="btn btn-sm btn-primary disabled" disabled>Bukti pembayaran telah diupload</a>
                            @endif
                        </div>
                        <div class="col-md-6">                            
                            <form action="{{ route('member.packet.activate', $paket->id) }}" method="POST" class="form form-inline float-right">
                                @csrf
                                @method('PUT')
                                <div class="form-group mr-4">
                                    <input type="text" class="form-control" name="access_code" placeholder="Input Kode Akses Paket Disini">
                                    @error('access_code')
                                        <br><span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-success btn-sm">activation</button>
                            </form>
                        </div>
                    </div>
                @else
                    <div class="row mt-2 float-right mr-1">
                        <a href="{{ route('member.detail_paket', ['id' => $paket->id]) }}" class="btn btn-outline-success mr-2">Lihat Detail</a>
                        <a href="{{ route('member.lesson', ['id' => $paket->id]) }}" class="btn btn-outline-primary">Lihat Course!</a>
                    </div>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload Bukti Pembayaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form action="{{ route('member.payment') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                <input type="hidden" name="packet_id" id="packet_id">
                <div class="form-group">
                    <input type="text" name="" id="packet_name" readonly class="form-control">
                </div>
                <div class="form-group">
                    <input type="file" name="file_receipt" class="form-control">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Bayar</button>
            </div>
        </form>
    </div>
  </div>
</div>

@endsection

@push('scripts')

<script>

$(document).ready(function(){
    $('.paymentBtn').click(function(){
        packet_id = $(this).data('paket');
        //console.log(packet_id);
        $('#packet_name').val($(this).data('name'));
        $('#packet_id').val(packet_id);
    });
    
});

</script>

@endpush