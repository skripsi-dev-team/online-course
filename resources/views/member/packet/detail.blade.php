@extends('member.app')

@section('title')
Detail Paket Saya
@endsection

@section('content')
<div class="container">
    <div class="col-md-12 mb-4">
        <h4 class="mb-3">Detail Paket</h4>
        
        
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ ucwords($packet->packet_name) }}</h6>
            </div>
            <div class="card-body">
                {!! $packet->description !!}
                <p>Kategori yang dipelajari</p>
                <ul>
                    @foreach($categories as $category)
                        <li>
                            {{ $category->category_name }}<br>
                            {!! $category->description !!}
                        </li>
                    @endforeach
                </ul>
                
            </div>
        </div>
       
    </div>
</div>
@endsection