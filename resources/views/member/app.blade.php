<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">

  <title> @yield('title') | TAW Course</title>

  <!-- Custom fonts for this template -->
  <link href="{{ asset('templates/admin-page/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{ asset('templates/admin-page/css/sb-admin-2.min.css') }}" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="{{ asset('templates/admin-page/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('templates/asset/style.css') }}">
  <link rel="stylesheet" href="{{ asset('templates/asset/font-awesome-4/css/font-awesome.min.css')}}">

</head>
<body class="page-member">

@include('member.navbar')
    <div id="app">

        @yield('content')
    </div>

@include('member.footer')

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Yakin akan logout?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">Klik logout untuk keluar dari aplikasi</div>
            <div class="modal-footer">
                <form action="{{ route('member.logout') }}" method="post">
                    @csrf
                    <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Logout</a>
                </form>
            </div>
        </div>
    </div>
</div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('templates/admin-page/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('templates/admin-page/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('templates/admin-page/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('templates/admin-page/js/sb-admin-2.min.js') }}"></script>

    <!-- Page level plugins -->
    <script src="{{ asset('templates/admin-page/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('templates/admin-page/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('templates/admin-page/js/demo/datatables-demo.js') }}"></script>

    <script>
            (function($){
                function pushAlert(response) {
                    var alert = $('#alertsDropdown');
                    var html = '';
                    
    
                    alert.find('.badge-counter').text(response.count);
    
                    if (response.count == 0) {
                        html += '<a class="dropdown-item d-flex align-items-center" href="#">';
                        html += '<div>Tidak ada pemberitahuan terbaru';
                        html += '</div></a>';
                    }else {
                        response.data.forEach(function(item) {
                            html += '<a class="dropdown-item d-flex align-items-center" href="#">';
                            html += '<div>';
                            html += '<div class="small text-gray-500">'+item.date+'</div>';
                            html += '<div class="font-weight-bold">'+item.title+'</div>';
                            html += '<div>'+item.text+'</div>';
                            html += '</div></a>';
                        });
                    }
    
                    $('div.dropdown-list').html(html);
                }
    
                function refreshNotif(response) {
                    var alert = $('#alertsDropdown');
                    alert.find('.badge-counter').text(response.count);
                }
    
                $('#alertsDropdown').on('click', function(e) {
                    
                    $.get('/api/member/refresh-notifications', refreshNotif);
                    
                });
    
                $.get('/api/member/notifications', pushAlert);
            })(jQuery)
        </script>

<script src="{{ asset('js/app.js') }}"></script>

    @stack('scripts')

</body>

</html>