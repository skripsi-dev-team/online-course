<div class="container-fluid bg-primary mt-5">
    <div class="container pt-5 pb-4">
        <div class="row">
            <div class="col-md-8">
                <h4 class="text-white">General Info</h4>
                <ul class="text-white footer-detail">
                    <li><i class="fa fa-map-marker"></i> &nbsp; Raya Padang Luwih No. 2D, Br. Gaji, Dalung, Badung - Bali</li>
                    <li><i class="fa fa-phone"></i>&nbsp; Telfon : +62 857 9207 8189 / +62 821 4703 1969</li>
                    <li><i class="fa fa-envelope"></i>&nbsp; Email : taw.course@gmail.com</li>
                </ul>
            </div>
            <div class="col-md-4">
                <h4 class="text-white">Follow Sosial Media Kami</h4>
                <ul class="text-white footer-detail footer-icon">
                    <li>
                        <a href="https://www.facebook.com/taw.course/" target="_blank" class="socmed-icon text-white">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/taw.course/" target="_blank" class="socmed-icon text-white">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                    
                </ul>
            </div>
            
        </div>
        <hr class="bg-white">
        <div class="row mt-5">
            <div class="col-md-12">
                <p class="text-center text-white">Copyright © 2019 TAW Accounting Education Centre Bali.</p>
            </div>
        </div>
    </div>
</div>