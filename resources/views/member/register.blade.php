@extends('tutor.app')

@section('title')
Register Member
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
            <div class="col-lg-5 d-none d-lg-block bg-register-member"></div>
            <div class="col-lg-7">
                <div class="p-5">
                    <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                    </div>
                    <form class="user" method="POST" action="{{ route('member.register') }}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-sm-12 mb-12 mb-sm-12">
                            <input type="text" class="form-control form-control-user" placeholder="Nama" name="name" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control form-control-user" placeholder="address Email" name="email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <input type="number" name="phone" class="form-control form-control-user" placeholder="No Telepon" value="{{ old('phone') }}">
                            @if ($errors->has('phone'))
                                <span class="text-danger">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <select name="packet_id" class="form-control form-control-user" style="padding: 0 1rem; height:100%">
                                <option value="">Pilih Paket</option>
                                @foreach ($packets as $item)
                                    <option value="{{ $item->id }}" <?php if(isset($_GET['paket'])) : ?>  
                                    <?php if($_GET['paket'] == $item->id) : ?>
                                        selected
                                        <?php endif ?>
                                    <?php endif ?>
                                    >{{ $item->packet_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="address" class="form-control" placeholder="address">{{ old('address') }}</textarea>
                        @if ($errors->has('address'))
                            <span class="text-danger">{{ $errors->first('address') }}</span>
                        @endif
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="password" class="form-control form-control-user" placeholder="Password" name="password">
                            @if ($errors->has('password'))
                                <span class="text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                        <div class="col-sm-6">
                            <input type="password" class="form-control form-control-user" placeholder="Repeat Password" name="password_confirmation">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                        Register Account
                    </button>
                    </form>
                    <hr>
                    <div class="text-center">
                        <a class="small" href="{{ route('member.login') }}">Already have an account? Login!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    </div>
</div>

@endsection