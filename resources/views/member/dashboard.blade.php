@extends('member.app')
@section('title')

Selamat Datang 

@endsection

@section('content')
<div class="container">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <div class="card mb-3 shadow" style="height: 150px">
                    <div class="card-header text-primary font-weight-bold"><i class="fas fa-atlas"></i> Total Paket</div>
                    <div class="card-body">
                        <h3 class="card-text">{{ $paket }} Paket</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 shadow" style="height: 150px">
                    <div class="card-header text-primary font-weight-bold"><i class="far fa-list-alt"></i> Total Materi</div>
                    <div class="card-body">
                        <h3 class="card-text">{{ $materi }} Materi</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3 shadow" style="height: 150px">
                    <div class="card-header text-primary font-weight-bold"><i class="fas fa-users"></i> Total Video Pembelajaran</div>
                    <div class="card-body">
                        <h3 class="card-text">{{ $video }} Video</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Selamat Datang</h6>
            </div>
            <div class="card-body">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente repellendus temporibus quam quaerat, eius dolores, omnis quidem deleniti explicabo quibusdam facilis eveniet dolorum rem accusantium aut, officia maxime. Quam, commodi!</p>
            </div>
        </div>
    </div>

</div>
@endsection