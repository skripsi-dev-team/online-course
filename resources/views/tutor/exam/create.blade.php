@extends('tutor.app')

@section('title')
Input Ujian {{ $packet->packet_name }}
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4">
    <div class="card-body">
    <p class="text-danger">Tiap paket hanya dapat memiliki 1 ujian</p>
        <form action="{{ route('exam.store', ['packet_id' => $packet->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
           
            <div class="form-group row">
                <div class="col-md-12">
                    <input type="file" name="file_exam" class="form-control">
                    @error('file_test')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="description">Deskripsi Ujian</label>
                    <textarea id="mytextarea" class="form-control form-control" name="description" placeholder="Deskripsi Ujian" >{{ old('description') }}</textarea>
                    @error('description')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <input type="submit" value="Simpan" class="btn btn-success ">
                </div>
            </div>
        </form>
    </div>
</div>



@endsection

@push('scripts')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'mytextarea' );
</script>

@endpush