@extends('tutor.app')

@section('title')
Data File Ujian {{ $packet->name }}
@endsection

@section('content')
<!-- Content -->

@if($exam == null)
<a href="{{ route('exam.create', ['packet_id' => $packet->id]) }}" class="btn btn-outline-primary mb-3">Tambah Ujian Pada Paket Ini</a>
@endif
<div class="card shadow mb-4">
                       
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
               
                <tbody>
                    @if($exam != null)
                    <tr>
                       <td>File Ujian</td>
                       <td><a href="{{ asset('storage/file_exam/'.$exam->file_exam) }}">Download</a></td>
                    </tr>
                    <tr>
                        <td>Deskripsi</td>
                        <td>{!! $exam->description !!}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>{{ $exam->date }}</td>
                    </tr>
                    <tr>
                       <td colspan="2">
                            <a href="{{ route('exam.answer', ['id' => $exam->id ]) }}" class="btn btn-outline-info btn-sm">Lihat Jawaban Member</a>
                            <a href="{{ route('exam.edit', ['id' => $exam->id]) }}" class="btn btn-warning btn-sm">Edit Ujian</a>
                       </td>
                    </tr>
                   
                   @else
                    Belum ada file ujian, mohon segera ditambahkan
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection


