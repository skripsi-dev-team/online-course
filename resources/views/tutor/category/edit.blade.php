@extends('tutor.app')

@section('title')
Edit Kategori
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4 col-md-6">
    <div class="card-body">
        <form action="{{ route('kategori.update', ['id' => $category->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" class="form-control form-control-user" name="category_name" placeholder="Nama Kategori"  value="{{ $category->category_name }}">
                    @error('category_name')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <select name="packet_id" class="form-control">
                        <option value="">- Pilih Paket -</option>
                        @foreach($packets as $packet)
                            <option value="{{ $packet->id }}" {{ $category->packet_id == $packet->id ? 'selected' : '' }} >{{ $packet->packet_name }}</option>
                        @endforeach
                    </select>
                    @error('packet_id')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="file_pdf">Materi</label>
                    <input type="file" name="file_pdf" class="form-control">
                    @error('file_pdf')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="description">Deskripsi</label>
                    <textarea name="description" id="mytextarea" class="form-control" placeholder="Masukan description Paket">{{ $category->description }}</textarea>
                    @error('description')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <!-- <div class="form-group row">
                <div class="col-md-12">
                    <label for="">Urutan Video</label>
                    <list-drag list="{{ $videos }}" name="title" sync-url="/api/switch-video"></list-drag>
                </div>
            </div> -->
           
            <div class="form-group row">
                <div class="col-md-12">
                    <input type="submit" value="Edit Kategori" class="btn btn-primary btn-user btn-block">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'mytextarea' );
</script>

@endpush