@extends('tutor.app')

@section('title')
Tambah Kategori
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4 col-md-6">
    <div class="card-body">
        <form action="{{ route('kategori.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" class="form-control form-control-user" name="category_name" placeholder="Nama Kategori"  value="{{ old('category_name') }}">
                    @error('category_name')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <select name="packet_id" class="form-control">
                        <option value="">- Pilih Paket -</option>
                        @foreach($packets as $packet)
                            <option value="{{ $packet->id }}">{{ $packet->packet_name }}</option>
                        @endforeach
                    </select>
                    <p>Paket anda tidak ada? <a href="" data-toggle="modal" data-target="#addPaketModal">Klik disini</a>  untuk menambahkan paket baru </p>
                    @error('packet_id')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="file_pdf">Materi PDF</label>
                    <input type="file" name="file_pdf" class="form-control">
                    @error('file_pdf')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="description">Deskripsi</label>
                    <textarea name="description" id="mytextarea" class="form-control" placeholder="Masukan description Paket">{{ old('description') }}</textarea>
                    @error('description')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
           
            <div class="form-group row">
                <div class="col-md-12">
                    <input type="submit" value="Tambah Kategori" class="btn btn-primary btn-user btn-block">
                </div>
            </div>
        </form>
    </div>
</div>

<!-- DETAIL Modal-->
<div class="modal fade" id="addPaketModal" tabindex="-1" role="dialog" aria-labelledby="addPaketModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailModalLabel">Tambah Paket</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('kategori.addpaket') }}" method="post">
                    @csrf
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control form-control-user" name="packet_name" placeholder="Nama Paket"  value="{{ old('packet_name') }}">
                            @error('packet_name')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <input type="number" class="form-control form-control-user" name="packet_price" placeholder="price Paket" value="{{ old('packet_price') }}">
                            @error('packet_price')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <textarea name="description" id="mytextarea" class="form-control" placeholder="Masukan description Paket">{{ old('description') }}</textarea>
                            @error('description')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <input type="hidden" name="tutor_id" value="{{ Auth::user()->id }}">
                            @error('tutor_id')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <input type="submit" value="Tambah Paket" class="btn btn-primary btn-user btn-block">
                        </div>
                    </div>
                </form>
            </div>
           
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>


<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'mytextarea' );
</script>

@endpush