@extends('tutor.app')

@section('title')
Data Kategori Paket
@endsection

@section('content')
<!-- Content -->

<a href="{{ route('kategori.create') }}" class="btn btn-outline-primary mb-3">Tambah Kategori</a>

<div class="card shadow mb-4">
                       
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-category" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Kategori</th>
                        <th>Paket</th>
                        <th>Materi</th>
                        <th>Jumlah Video</th>
                        <th>Action</th>
                    </tr>
                </thead>
            
                <tbody>
                    @foreach($categories as $row)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $row->category_name }}</td>
                        <td>{{ $row->packet->packet_name }}</td>
                        <td><a href="{{ asset('storage/file_pdf/'.$row->file_pdf) }}">Download</a></td>
                        <td>{{ $row->videos->count() }}</td>
                        <td>
                            <!-- <a href="{{ route('video.index', ['id_kategori' => $row->id ]) }}" class="btn btn-outline-success btn-sm">Lihat Video</a>
                            <a href="{{ route('kategori.test', ['id_kategori' => $row->id ]) }}" class="btn btn-outline-primary btn-sm">Lihat Test</a> -->
                            <a href="{{ route('kategori.view', ['id_kategori' => $row->id ]) }}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                            <a href="{{ route('kategori.edit', ['id' => $row->id]) }}" class="btn btn-warning btn-sm"><i class="fa fa-cog"></i></a>
                            <button value="{{ $row->id }}" class="btn btn-danger btn-sm delete-btn" data-toggle="modal" data-id="{{ $row->id }}" data-target="#deleteModal"><i class="fa fa-trash"></i></button>
                        </td>
                       
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- DETAIL Modal-->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailModalLabel">Detail Paket</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table detail-table">
                    <tr>
                        <td>Deskripsi</td>
                    </tr>  
                    <tr>
                        <td class="description"></td>
                    </tr>
                    <tr>
                        <td class="number_ofvideos"></td>
                    </tr>
                    <tr>
                        <td class="packet_name"></td>
                    </tr>

                </table>
            </div>
            <div class="modal-footer">
                <a class="btn btn-light" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>

<!-- Delete Modal-->
<div class="modal fade deleteModal" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Yakin hapus paket?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">Paket yang sudah dihapus tidak dapat dikembalikan</div>
            <!-- <input type="text" class="unique"> -->
        
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger btn-del">Hapus</button>
            </div>
           
        </div>
    </div>
</div>

@endsection

@push('scripts')

<script>

$(document).ready(function(){
    $('.delete-btn').click(function(){
        var id = $(this);
       console.log(id.data('id'));
      // $('.unique').val(id.data('id'));
        $('.btn-del').click(function(){
            $.ajax({
                url : "kategori/"+id.data('id'),
                method : "POST",
                data : {
                    _token : "{{csrf_token()}}",
                    _method : "DELETE",
                }
            })
            .then(function(data){
               // console.log(data);
                location.reload();
            });
        });
    });

});

</script>

@endpush