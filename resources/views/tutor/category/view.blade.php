@extends('tutor.app')

@section('title')
Detail Kategori Paket
@endsection

@section('content')
<!-- Content -->

<div class="card shadow mb-4">
                       
    <div class="card-body">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-category-tab" data-toggle="tab" href="#nav-category" role="tab" aria-controls="nav-category" aria-selected="true">Kategori</a>
                <a class="nav-item nav-link" id="nav-video-tab" data-toggle="tab" href="#nav-video" role="tab" aria-controls="nav-video" aria-selected="false">Video</a>
                <a class="nav-item nav-link" id="nav-test-tab" data-toggle="tab" href="#nav-test" role="tab" aria-controls="nav-test" aria-selected="false">Test</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-category" role="tabpanel" aria-labelledby="nav-category-tab">
                <table class="table table-striped">
                    <tr>
                        <td width="20%">Nama Kategori</td>
                        <td width="5%">:</td>
                        <td>{{ $categories->category_name }}</td>
                    </tr>
                    <tr>
                        <td>Paket</td>
                        <td>:</td>
                        <td>{{ $categories->packet->packet_name }}</td>
                    </tr>
                    <tr>
                        <td>Materi</td>
                        <td>:</td>
                        <td><a href="{{ asset('storage/file_pdf/'.$categories->file_pdf) }}">Download</a></td>
                    </tr>
                    <tr>
                        <td>Jumlah Video</td>
                        <td>:</td>
                        <td>{{ $categories->videos->count() }}</td>
                    </tr>
                    <tr>
                        <td>Deskripsi</td>
                        <td>:</td>
                        <td>{!! $categories->description !!}</td>
                    </tr>
                </table>
            </div>

            <div class="tab-pane fade" id="nav-video" role="tabpanel" aria-labelledby="nav-video-tab">
                <br>
                <a href="{{ route('video.create', ['id_kategori' => $categories->id]) }}" class="btn btn-outline-primary">Tambah Video</a>
                <hr>

                <table class="table table-bordered video-table" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Action</th>             
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($videos as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->title }}</td>
                            <td>
                                <a href="{{ route('comment.show', ['id_kategori' => $categories->id, 'id' => $row->id]) }}" class="btn btn-outline-success btn-sm">Lihat comment</a>
                                <button value="{{ $row->id }}"  class="btn btn-info btn-sm detail-btn-video" data-toggle="modal" data-target="#detailModalVideo"><i class="fa fa-eye"></i></button>
                                <a href="{{ route('video.edit', ['id_kategori' => $categories->id, 'id' => $row->id]) }}" class="btn btn-warning btn-sm"><i class="fa fa-cog"></i></a>
                                <button value="{{ $row->id }}" class="btn btn-danger btn-sm delete-btn-video" data-toggle="modal" data-id="{{ $row->id }}" data-target="#deleteModal"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                
                </table>
            </div>

            <div class="tab-pane fade" id="nav-test" role="tabpanel" aria-labelledby="nav-test-tab">
                <br>
                @if($tests == null)
                    <a href="{{ route('test.create', ['id_kategori' => $categories->id]) }}" class="btn btn-outline-primary mb-3">Tambah Test Pada Kategori Ini</a>
                @endif
                <hr>

                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    
                        <tbody>
                            @if($tests != null)
                            <tr>
                            <td>File Test</td>
                            <td><a href="{{ asset('storage/file_test/'.$tests->file_test) }}">Download</a></td>
                            </tr>
                            <tr>
                                <td>Deskripsi</td>
                                <td>{!! $tests->description !!}</td>
                            </tr>
                            <tr>
                                <td>Tanggal</td>
                                <td>{{ $tests->date }}</td>
                            </tr>
                            <tr>
                            <td colspan="2">
                                    <a href="{{ route('test.answer', ['id' => $tests->id ]) }}" class="btn btn-outline-info btn-sm">Lihat Jawaban Member</a>
                                    <a href="{{ route('test.edit', ['id' => $tests->id]) }}" class="btn btn-warning btn-sm">Edit Test</a>
                            </td>
                            </tr>
                        
                        @else
                            Belum ada test, mohon segera ditambahkan
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>


<!-- DETAIL Modal-->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailModalLabel">Detail Paket</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table detail-table">
                    <tr>
                        <td>Deskripsi</td>
                    </tr>  
                    <tr>
                        <td class="description"></td>
                    </tr>
                    <tr>
                        <td class="number_ofvideos"></td>
                    </tr>
                    <tr>
                        <td class="packet_name"></td>
                    </tr>

                </table>
            </div>
            <div class="modal-footer">
                <a class="btn btn-light" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>

<!-- Delete Modal-->
<div class="modal fade deleteModal" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Yakin hapus paket?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">Paket yang sudah dihapus tidak dapat dikembalikan</div>
            <!-- <input type="text" class="unique"> -->
        
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger btn-del">Hapus</button>
            </div>
           
        </div>
    </div>
</div>

<!-- view video -->
<div class="modal fade" id="detailModalVideo" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailModalLabel">Detail Video</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table detail-table">
                    <tr>
                        <td class="embed_link">
                            <iframe class="embed_video" width="560" height="315" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </td>
                    </tr>
                    <tr>
                        <td>Deskripsi</td>
                    </tr>  
                    <tr>
                        <td class="description"></td>
                    </tr>
                

                </table>
            </div>
            <div class="modal-footer">
                <a class="btn btn-light" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')

<script>

$(document).ready(function(){
    $('.delete-btn').click(function(){
        var id = $(this);
       console.log(id.data('id'));
      // $('.unique').val(id.data('id'));
        $('.btn-del').click(function(){
            $.ajax({
                url : "kategori/"+id.data('id'),
                method : "POST",
                data : {
                    _token : "{{csrf_token()}}",
                    _method : "DELETE",
                }
            })
            .then(function(data){
               // console.log(data);
                location.reload();
            });
        });
    });

});

$(document).on('click', '.detail-btn-video', function(){
    var video_id = $(this).val();

    $.get('video/'+video_id, function(data){
        console.log(data);
        $('.modal-title').html(data.title);
        $('.embed_video').attr("src", data.embed_link);
        $('.description').html(data.description);      
       
    });

});

$(document).ready(function(){
    $('.delete-btn-video').click(function(){
        var video_id = $(this).val();
        var category_id = "{{ $categories->id }}";
        $('.btn-del').click(function(){
            $.ajax({
                url : "/tutor/kategori/"+category_id+"/video/"+ video_id,
                method : "POST",
                data : {
                    _token : "{{csrf_token()}}",
                    _method : "DELETE",
                }
            })
            .then(function(data){
               // console.log(data);
                location.reload();
            });
        });
    });

});

</script>

@endpush