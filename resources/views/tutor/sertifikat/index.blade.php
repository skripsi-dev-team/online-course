@extends('tutor.app')

@section('title')
Data Sertifikat
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4">
                       
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>                
                        <th>No Sertifikat</th>
                        <th>Nama Member</th>
                        <th>Paket</th>
                        <th>Tanggal</th>
                        <th>Download</th>                       
                    </tr>
                </thead>
            
                <tbody>
                    @foreach($sertifikat as $row)
                       
                       <tr>                      
                            <td>{{ $row->certificate_number }}</td>
                            <td>{{ $row->member->name }}</td>
                            <td>{{ $row->packet->packet_name }}</td>
                            <td>{{ $row->created_at }}</td>
                            <td>
                                <a href="{{ route('tutor.generate-sertifikat', ['member_id' => $row->member_id, 'packet_id' => $row->packet_id, 'certificate_number' => $row->certificate_number]) }}" target="_blank"> Download </a>
                                
                            </td>
                         
                        </tr>                 
          
                       
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection




