<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sertifikat - TAW Course </title>
    <!-- Custom styles for this template -->
    <link href="{{ asset('templates/admin-page/css/sb-admin-2.min.css') }}" rel="stylesheet">

    <style>
    
        body {
            background-image:url('/image/bg1.jpeg');
            background-size: cover;
            background-repeat: no-repeat;
            box-shadow:inset 0 0 0 2000px rgba(26, 21, 24, 0.58);
            width:100%;
            height:100vh;
        }

        .bordered {
            border: 1px solid white;
            padding: 0;
            border-radius: 25px;
        }

        .no-border {
            border: 0!important;
        }

    </style>
</head>
<body>
    <div class="container">
        <div class="row pt-3 pb-3">
            <div class="col-md-12 text-center">
                <img src="{{ asset('image/logo.png') }}" alt="logo">
            </div>
        </div>

        <div class="row justify-content-center mt-3">
            <div class="col-md-6">
                <h4 class="text-white text-center">Sertifikat</h4>
            </div>
        </div>
        
        <div class="row justify-content-center">
            <div class="col-md-6 bordered">
                <table class="table text-white">
                    <tr style="border-top:0px">
                        <td class="no-border">Nomor SK</td>
                        <td class="no-border">:</td>
                        <td class="no-border"><?= $certificate->certificate_number ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td><?= $certificate->date ?></td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td><?= $certificate->member->name ?></td>
                    </tr>
                    <tr>
                        <td>Paket</td>
                        <td>:</td>
                        <td><?= $certificate->packet->packet_name ?></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row justify-content-center mt-5">
            <div class="col-md-6 text-center text-white">
               <p>Sertifikat terbaru menggunakan tanda tangan digital dan sudah terjamin keasliannya.</p>
            </div>
        </div>
    </div>
</body>
</html>