<style>


.page-sertifikat {
    height: 100vh;
}

h1.heading-sertifikat {
    font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
    font-size: 45px;
    margin-top: 0;
    font-weight: 500;
    text-align: center;
    margin-bottom: 10px;
}

.border-sertifikat {
    border: 13px double #4e73df;
    padding: 20px;
    border-radius: 20px;
}

.text-sertifikat {
    font-family: Arial, Helvetica, sans-serif;
    line-height: 1.5em;
    text-align: center;
}
.inner-sertifikat ul {
    padding-left: 0;
    list-style: none;
}

.nama-peserta {
    font-size: 28px;
    font-family: Segoe UI;
    text-transform: uppercase;
    font-weight: 500;
    border-bottom: 1px solid blue;
    padding-bottom: 17px;
}
.body-sertifikat {
    background-image: url(/image/bg-sertif.png);
    background-size: 40%;
    background-repeat: no-repeat;
    background-position: 50%;
}

.qr-img {
    margin: 0!important;
}
</style>

<div class="border-sertifikat">
    <div class="body-sertifikat">
        <h1 class="heading-sertifikat">Sertifikat</h1>
        <p style="text-align:center;" class="qr-img"><img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->generate(route('sertifikat.show', $certificate_number))) }}" alt="qrcode"></p>
        <hr>
        <div class="text-sertifikat">
            <h4>Diberikan Kepada</h4>
            <div class="nama-peserta">
                {{ $member->name }}
            </div>
            <div class="inner-sertifikat">
                <p>Telah menyelesaikan {{ $packet->packet_name }} kursus akuntansi yang dilaksanakan oleh TAW Course dengan kategori pembelajaran sebagai berikut: </p>
                <ul>
                    @foreach($packet->categories as $row)
                        <li> {{ $row->category_name }}</li>
                    @endforeach
                </ul>
                <p>Dengan demikian member tersebut dinyatakan LULUS dan siap untuk terjun ke dunia kerja</p>
            </div>
            <div class="footer-sertifikat">
                Denpasar, 22-07-2019 <br><br>
                TTD <br><br>
                TAW Course.
            </div>
        </div>
    </div>
</div>
