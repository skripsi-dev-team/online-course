<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/tutor/dashboard">
        <!-- <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">TAW Course</div> -->
        <img src="{{ asset('image/logo.png') }}" alt="" width="90%">
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ urlHasPrefix('dashboard') ? 'active' : '' }}">
        <a class="nav-link" href="/tutor/dashboard">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Master Data
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ urlHasPrefix('paket') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{ route('paket.index') }}">
            <i class="fas fa-fw fa-book"></i>
            <span>Paket</span>
        </a>
    </li>
    
    <li class="nav-item {{ urlHasPrefix('kategori') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{ route('kategori.index') }}">
            <i class="fas fa-fw fa-address-book"></i>
            <span>Kursus</span>
        </a>
    </li>

    <li class="nav-item {{ urlHasPrefix('member') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{ route('tutor.member') }}">
            <i class="fas fa-fw fa-users"></i>
            <span>Member</span>
        </a>
    </li>

    <li class="nav-item {{ urlHasPrefix('payment') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{ route('tutor.payment') }}">
            <i class="fas fa-fw fa-receipt"></i>
            <span>Pembayaran</span>
        </a>
    </li>

    <li class="nav-item {{ urlHasPrefix('sertifikat') ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{ route('tutor.sertifikat') }}">
            <i class="fas fa-fw fa-award"></i>
            <span>Sertifikat</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
            <i class="fas fa-fw fa-file"></i>
            <span>Nilai</span>
        </a>
        <div id="collapsePages" class="collapse {{ urlHasPrefix('nilai') ? 'show' : '' }}" aria-labelledby="headingPages" data-parent="#accordionSidebar" style="">
            <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Laporan Nilai</h6>
            <a class="collapse-item {{ urlHasPrefix('nilai-test') ? 'active' : '' }}" href="{{ route('tutor.nilai.test') }}">Nilai Test</a>
            <a class="collapse-item {{ urlHasPrefix('nilai-ujian') ? 'active' : '' }}" href="{{ route('tutor.nilai.ujian') }}">Nilai Ujian</a>
            
            </div>
        </div>
    </li>




    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->