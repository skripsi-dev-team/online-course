@extends('tutor.app')

@section('title')
    Data Tutor
@endsection

@section('content')
    <!-- Content -->

    <a href="{{ route('tutor.create') }}" class="btn btn-outline-primary mb-3">Tambah Tutor</a>

    <div class="card shadow mb-4">

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-category" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Tutor</th>
                        <th>Email</th>
                        <th>No Telp</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($tutors as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->username }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->phone }}</td>
                            <td>
                                <a href="{{ route('tutor.edit', ['id' => $row->id]) }}" class="btn btn-warning btn-sm"><i class="fa fa-cog"></i></a>
                                <button value="{{ $row->id }}" class="btn btn-danger btn-sm delete-btn" data-toggle="modal" data-id="{{ $row->id }}" data-target="#deleteModal"><i class="fa fa-trash"></i></button>
                            </td>

                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Delete Modal-->
    <div class="modal fade deleteModal" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Yakin hapus paket?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Tutor yang sudah dihapus tidak dapat dikembalikan</div>
                <!-- <input type="text" class="unique"> -->

                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-del">Hapus</button>
                </div>

            </div>
        </div>
    </div>

@endsection

@push('scripts')

    <script>


        $(document).ready(function(){
            $('.delete-btn').click(function(){
                var id = $(this);
                console.log(id.data('id'));
                // $('.unique').val(id.data('id'));
                $('.btn-del').click(function(){
                    $.ajax({
                        url : "tutor/"+id.data('id'),
                        method : "POST",
                        data : {
                            _token : "{{csrf_token()}}",
                            _method : "DELETE",
                        }
                    })
                        .then(function(data){
                            // console.log(data);
                            location.reload();
                        });
                });
            });

        });

    </script>

@endpush