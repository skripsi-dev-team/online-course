@extends('tutor.app')

@section('title')
Selamat Datang Tutor
@endsection

@section('content')
<!-- Content -->

<!-- paket -->
<div class="row">
    <div class="col-md-4">
        <div class="card text-white bg-primary mb-3" style="height: 150px">
            <div class="card-header bg-primary"><i class="fas fa-atlas"></i> Total Paket</div>
            <div class="card-body">
                <h3 class="card-text">{{ $packet }} Paket</h3>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card text-white bg-primary mb-3" style="height: 150px">
            <div class="card-header bg-primary"><i class="far fa-list-alt"></i> Total Kategori</div>
            <div class="card-body">
                <h3 class="card-text">{{ $category }} Kategori</h3>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card text-white bg-primary mb-3" style="height: 150px">
            <div class="card-header bg-primary"><i class="fas fa-users"></i> Total Member</div>
            <div class="card-body">
                <h3 class="card-text">{{ $member }} Orang</h3>
            </div>
        </div>
    </div>
</div>

<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
           <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis possimus veritatis magni! Perferendis voluptas eius alias illo est rerum ipsum cupiditate soluta doloremque deleniti. Debitis temporibus velit blanditiis explicabo quo?</p>
           <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis possimus veritatis magni! Perferendis voluptas eius alias illo est rerum ipsum cupiditate soluta doloremque deleniti. Debitis temporibus velit blanditiis explicabo quo?</p>
        </div>
    </div>
</div>


@endsection
