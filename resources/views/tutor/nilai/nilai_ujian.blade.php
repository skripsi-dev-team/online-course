@extends('tutor.app')

@section('title')
Nilai Ujian Member 
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4">
                       
    <div class="card-body">
    <a href="{{ route('nilai.ujian.print') }}" class="btn btn-secondary"><i class="fa fa-print"></i> Print Nilai Ujian</a>
        <table class="table" id="dataTable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Member</th>
                    <th>Ujian Paket</th>
                    <th>Nilai</th>
                    <th>Status</th>
                    <th>Detail</th>
                </tr>
            </thead>
            <tbody>
                @foreach($nilai as $row)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row->member->name }}</td>
                    <td>{{ $row->exam->packet->packet_name }}</td>
                    <td>{{ $row->score }}</td>
                    <td>{!! $row->scoreStatus() !!}</td>
                    <td><a href="{{ route('exam.answer', ['id' => $row->exam->id ]) }}" class="btn btn-light btn-sm">Detail</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection
           