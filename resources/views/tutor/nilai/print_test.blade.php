<style>

body {
    font-family: Arial, Helvetica, sans-serif;
}

.header-print {
    text-align:center;
}
table {
  border-collapse: collapse;
}

table, th, td {
  border: 1px solid black;
}
</style>

<div class="container-print">
    <div class="header-print">
        <h3>TAW Accounting Education Center</h3>
        <p>Raya Padang Luwih No. 2D, Br. Gaji, Dalung, Badung - Bali</p>
        <p>Telfon : +62 857 9207 8189 / +62 821 4703 1969</p>
        <p>Email : taw.course@gmail.com</p>
        <hr>
    </div>
    <div class="body-print">
        <h4>Data Nilai Test Member</h4>

        <table class="table table-bordered table-category"  width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Member</th>
                    <th>Kategori Test</th>
                    <th>Nilai</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($nilai as $row)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row->member->name }}</td>
                    <td>{{ $row->test->category->category_name }}</td>
                    <td>{{ $row->score }}</td>
                    <td>{!! $row->scoreStatus() !!}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
