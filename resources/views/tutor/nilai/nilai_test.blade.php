@extends('tutor.app')

@section('title')
Nilai Test Member 
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4">
                       
    <div class="card-body">
    <a href="{{ route('nilai.test.print') }}" class="btn btn-secondary"><i class="fa fa-print"></i> Print Nilai Test</a>
        <table class="table" id="dataTable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Member</th>
                    <th>Kategori Test</th>
                    <th>Nilai</th>
                    <th>Status</th>
                    <th>Detail</th>
                </tr>
            </thead>
            <tbody>
                @foreach($nilai as $row)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row->member->name }}</td>
                    <td>{{ $row->test->category->category_name }}</td>
                    <td>{{ $row->score }}</td>
                    <td>{!! $row->scoreStatus() !!}</td>
                    <td><a href="{{ route('test.answer', ['id' => $row->test->id ]) }}" class="btn btn-light btn-sm">Detail</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection