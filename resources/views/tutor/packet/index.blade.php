@extends('tutor.app')

@section('title')
Data Paket
@endsection

@section('content')
<!-- Content -->

<a href="{{ route('paket.create') }}" class="btn btn-outline-primary mb-3">Tambah Paket</a>

<div class="card shadow mb-4">
                       
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Paket</th>
                        <th>Jml Kategori</th>
                        <th>Harga</th>
                        <th>Tutor</th>
                        <th>Best Selling</th>
                        <th>Action</th>
                    </tr>
                </thead>
            
                <tbody>
                    @foreach($packets as $row)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $row->packet_name }}</td>
                        <td>{{ $row->categories->count() }}</td>
                        <td>Rp. {{ number_format($row->packet_price) }}</td>
                        <td>{{ $row->tutor->name }}</td>
                        <td>{!! $row->checkBestSelling() !!}</td>
                        <td>
                            <a href="{{ route('exam.index', ['packet_id' => $row->id]) }}" class="btn btn-sm btn-outline-success">Ujian</a>
                            <button value="{{ $row->id }}" id="detail-btn" class="btn btn-info btn-sm" data-toggle="modal" data-tutor="{{ $row->tutor->name }}" data-target="#detailModal"><i class="fa fa-eye"></i></button>
                            <a href="{{ route('paket.edit', ['id' => $row->id]) }}" class="btn btn-warning btn-sm"><i class="fa fa-cog"></i></a>
                            <button value="{{ $row->id }}" class="btn btn-danger btn-sm delete-btn" data-toggle="modal" data-id="{{ $row->id }}" data-target="#deleteModal"><i class="fa fa-trash"></i></button>
                        </td>
                       
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- DETAIL Modal-->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailModalLabel">Detail Paket</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table detail-table">
                    <tr>
                        <td>Deskripsi</td>
                    </tr>  
                    <tr>
                        <td class="description"></td>
                    </tr>
                    <tr>
                        <td class="number_of_categories"></td>
                    </tr>
                    <tr>
                        <td>
                            Kategori yang dibahas: 
                            <ul class="category_ul">
                               
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td class="price"></td>
                    </tr>
                    <tr>
                        <td class="nama_tutor"></td>
                    </tr>

                </table>
            </div>
            <div class="modal-footer">
                <a class="btn btn-light" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>

<!-- Delete Modal-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Yakin hapus paket?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">Paket yang sudah dihapus tidak dapat dikembalikan</div>
            
        
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger btn-del">Hapus</button>
            </div>
           
        </div>
    </div>
</div>

@endsection

@push('scripts')

<script>

$(document).on('click', '#detail-btn', function(){
    var id = $(this).val();
    var button = $(this);

    $.get('paket/'+id, function(result){
        console.log(result);
        $('.modal-title').html(result.data.packet.packet_name);
        $('.description').html(result.data.packet.description);
        $('.price').html("Rp. " + result.data.packet.packet_price);
        $('.number_of_categories').html("Jumlah kategori : "+result.data.packet.number_of_categories)
        $('.nama_tutor').html(button.data('tutor'));
        var categoryLength = result.data.categories.length;
        var html = '';
        $.each(result.data.categories, function(index, value){
            html += "<li>" + value.category_name +"</li>";
            
        });
        $('.category_ul').html(html);

    });

});


$(document).ready(function(){
    $('.delete-btn').click(function(){
        var id = $(this);
        $('.id_paket').val(id.data('id'));
       
        $('.btn-del').click(function(){
            $.ajax({
                url : "paket/"+id.data('id'),
                method : "POST",
                data : {
                    _token : "{{csrf_token()}}",
                    _method : "DELETE",
                }
            })
            .then(function(data){
                location.reload();
            });
        });
    });

});

</script>

@endpush