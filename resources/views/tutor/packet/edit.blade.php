@extends('tutor.app')

@section('title')
Edit Paket
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4 col-md-6">
    <div class="card-body">
        <form action="{{ route('paket.update', ['id' => $packet->id ]) }}" method="post">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" class="form-control form-control-user" name="packet_name" placeholder="Nama Paket" value="{{ $packet->packet_name }}">
                    @error('packet_name')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="number" class="form-control form-control-user" name="packet_price" placeholder="price Paket" value="{{ $packet->packet_price }}">
                    @error('price')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                <label for="description">description</label>
                    <textarea name="description" id="mytextarea" class="form-control" placeholder="Masukan description Paket">{{ $packet->description }}</textarea>
                    @error('description')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <!-- <div class="form-group row">
                <div class="col-md-12">
                    <label for="">Urutan Kategori</label>
                    <list-drag list="{{ $categories }}" name="category_name" sync-url="/api/switch-category"></list-drag>
                </div>
            </div> -->

            <div class="form-group row">
                <div class="col-md-12">
                    <label for="">Best Selling</label>
                    <select name="best_selling" class="form-control">
                        <option value=""> - Pilih - </option>
                        <option value="1" {{ $packet->best_selling == 1 ? 'selected' : '' }}>True</option>
                        <option value="0" {{ $packet->best_selling == 0 ? 'selected' : '' }}>False</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-12">
                    <input type="submit" value="Edit Paket" class="btn btn-primary btn-user btn-block">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'mytextarea' );
</script>

@endpush