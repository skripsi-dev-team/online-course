@extends('tutor.app')

@section('title')
Tambah Paket
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4 col-md-6">
    <div class="card-body">
        <form action="{{ route('paket.store') }}" method="post">
            @csrf
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" class="form-control form-control-user" name="packet_name" placeholder="Nama Paket"  value="{{ old('packet_name') }}">
                    @error('packet_name')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="number" class="form-control form-control-user" name="packet_price" placeholder="Harga Paket" value="{{ old('price') }}">
                    @error('price')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="description">Deskripsi</label>
                    <textarea name="description" id="mytextarea" class="form-control" placeholder="Masukan description Paket">{{ old('description') }}</textarea>
                    @error('description')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-12">
                    <label for="">Best Selling</label>
                    <select name="best_selling" class="form-control">
                        <option value=""> - Pilih - </option>
                        <option value="1">True</option>
                        <option value="0">False</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-12">
                    <input type="submit" value="Tambah Paket" class="btn btn-primary btn-user btn-block">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>


<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'mytextarea' );
</script>

@endpush