@extends('tutor.app')

@section('title')
Data Video Kategori {{ $category->category_name }}
@endsection

@section('content')
<!-- Content -->

<a href="{{ route('video.create', ['id_kategori' => $category->id]) }}" class="btn btn-outline-primary mb-3">Tambah Video</a>

<div class="card shadow mb-4">
                       
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered video-table" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Action</th>             
                    </tr>
                </thead>
                <tbody>
                    @foreach($videos as $row)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $row->title }}</td>
                        <td>
                            <a href="{{ route('comment.show', ['id_kategori' => $category->id, 'id' => $row->id]) }}" class="btn btn-outline-success btn-sm">Lihat comment</a>
                            <button value="{{ $row->id }}"  class="btn btn-info btn-sm detail-btn" data-toggle="modal" data-target="#detailModal"><i class="fa fa-eye"></i></button>
                            <a href="{{ route('video.edit', ['id_kategori' => $category->id, 'id' => $row->id]) }}" class="btn btn-warning btn-sm"><i class="fa fa-cog"></i></a>
                            <button value="{{ $row->id }}" class="btn btn-danger btn-sm delete-btn" data-toggle="modal" data-id="{{ $row->id }}" data-target="#deleteModal"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
               
            </table>
        </div>
    </div>
</div>


<!-- DETAIL Modal-->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailModalLabel">Detail Paket</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table detail-table">
                    <tr>
                        <td class="embed_link">
                            <iframe class="embed_video" width="560" height="315" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </td>
                    </tr>
                    <tr>
                        <td>description</td>
                    </tr>  
                    <tr>
                        <td class="description"></td>
                    </tr>
                

                </table>
            </div>
            <div class="modal-footer">
                <a class="btn btn-light" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>

<!-- Delete Modal-->
<div class="modal fade deleteModal" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Yakin hapus paket?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <div class="modal-body">Paket yang sudah dihapus tidak dapat dikembalikan</div>
            <!-- <input type="text" class="unique"> -->
        
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger btn-del">Hapus</button>
            </div>
           
        </div>
    </div>
</div>

@endsection

@push('scripts')

<script>

$(document).on('click', '.detail-btn', function(){
    var video_id = $(this).val();

    $.get('video/'+video_id, function(data){
        console.log(data);
        $('.modal-title').html(data.title);
        $('.embed_video').attr("src", data.embed_link);
        $('.description').html(data.description);      
       
    });

});


$(document).ready(function(){
    $('.delete-btn').click(function(){
        var video_id = $(this).val();
        var category_id = "{{ $category->id }}";
        $('.btn-del').click(function(){
            $.ajax({
                url : "/tutor/kategori/"+category_id+"/video/"+ video_id,
                method : "POST",
                data : {
                    _token : "{{csrf_token()}}",
                    _method : "DELETE",
                }
            })
            .then(function(data){
               // console.log(data);
                location.reload();
            });
        });
    });

});

</script>

@endpush