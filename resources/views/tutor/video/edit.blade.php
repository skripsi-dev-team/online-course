@extends('tutor.app')

@section('title')
Edit Video
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4 col-md-6">
    <div class="card-body">
        <form action="{{ route('video.update', ['id_kategori' => $id_kategori, 'id' => $video->id]) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <input type="hidden" name="category_id" value="{{ $id_kategori }}">
            <input type="hidden" id="slug" class="form-control" placeholder="Masukan Slug" name="slug" value="{{ $video->slug }}">
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="title">Title Video</label>
                    <input type="text" class="form-control form-control-user title" name="title" placeholder="Title Video"  value="{{ $video->title }}" onkeyup="makeSlug()">
                    @error('title')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="embed_link">Embed Link</label>
                    <input type="text" class="form-control form-control-user" name="embed_link" placeholder="Embed Link Video"  value="{{ $video->embed_link }}">
                    @error('embed_link')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="description">description</label>
                    <textarea name="description" id="mytextarea" class="form-control" placeholder="Masukan description Paket">{{ $video->description }}</textarea>
                    @error('description')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <input type="submit" value="Edit Video" class="btn btn-primary btn-user btn-block">
                </div>
            </div>
        </form>
    </div>
</div>


@endsection

@push('scripts')
<script>

function makeSlug(){
    var titleSlug = $('.title').val();
    var slugName = titleSlug.split(' ').join('-').toLowerCase();
    $('#slug').val(slugName);
}
</script>


<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>


<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'mytextarea' );
</script>

@endpush