@extends('tutor.app')

@section('title')
Data comment Video {{ $video->title }}
@endsection

@section('content')
<!-- Content -->

<div class="card shadow mb-4">
                       
    <div class="card-body">
        <ul class="list-comment">
            @if(count($comments) != 0);
                @foreach($comments as $comment)
                    <li>
                        <div class="username">
                            {{ $comment->member->name }}
                        </div>
                        <small><i>{{ $comment->date }}</i></small>
                        <div class="comment">
                            {{ $comment->comment }}
                        </div>
                        <hr>
                        <div class="row-btn">
                            <form action="{{ route('comment.delete', ['id' => $comment->id]) }}" method="post">
                                @csrf 
                                @method('DELETE')
                                <a href="#" class="btn btn-sm btn-success btn-reply" data-toggle="modal" data-target="#replyModal" data-id="{{ $comment->id }}" data-member="{{ $comment->member_id }}" data-comment="{{ $comment->comment }}">Balas comment</a>
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin hapus comment?')">Hapus comment</button>
                            </form>
                        </div>
                    </li>
                    @if($comment->replies)
                        @foreach($comment->replies as $reply)
                        <li class="reply">
                            <div class="username">
                            {{ $reply->tutor->name }} <small>(Tutor)</small>
                            </div>
                            <small><i>{{ $comment->date }}</i></small>
                            <div class="comment mt-3">
                                {{ $reply->comment }}
                            </div>
                        </li>
                        @endforeach
                    @endif
                @endforeach
            @else 
            <li>Tidak Ada comment</li>
            @endif
        </ul>
        
    </div>
</div>

<!-- REPLY Modal-->
<div class="modal fade" id="replyModal" tabindex="-1" role="dialog" aria-labelledby="replyModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="replyModalLabel">Balas comment</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-secondary reply">
                </div>
                <form action="{{ route('comment.reply') }}" method="post">
                    @csrf
                    <input type="hidden" name="reply_id" class="reply_id" value="">
                    <input type="hidden" name="video_id" value="{{ $video->id }}">
                    <input type="hidden" name="member_id" class="member_id" value="">
                    <div class="form-group">
                        <label for="comment">Balas</label>
                        <textarea name="comment" class="form-control" placeholder="Masukan comment disini"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Simpan">
                    </div>
                </form>
            </div>
           
        </div>
    </div>
</div>

@endsection

@push('scripts')

<script>

$(document).ready(function(){
    $('#replyModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var reply_id = button.data('id'); // Extract info from data-* attributes
        var comment = button.data('comment');
        var member_id = button.data('member');
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('.reply').text(comment);
        modal.find('.reply_id').val(reply_id);
        modal.find('.member_id').val(member_id);
    });
});

</script>

@endpush