@extends('tutor.app')

@section('title')
Tambah Video
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4 col-md-6">
    <div class="card-body">
        <form action="{{ route('video.store', ['id_kategori' => $id_kategori]) }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="category_id" value="{{ $id_kategori }}">
            <input type="hidden" id="slug" class="form-control" placeholder="Masukan Slug" name="slug" value="{{ old('slug') }}">
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" class="form-control form-control-user title" name="title" placeholder="Title Video"  value="{{ old('title') }}" onkeyup="makeSlug()">
                    @error('title')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <input type="text" class="form-control form-control-user" name="embed_link" placeholder="Embed Link Video"  value="{{ old('embed_link') }}">
                    @error('embed_link')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <label for="description">Deskripsi</label>
                    <textarea name="description" id="mytextarea" class="form-control" placeholder="Masukan description Paket">{{ old('description') }}</textarea>
                    @error('description')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <input type="submit" value="Tambah Video" class="btn btn-primary btn-user btn-block">
                </div>
            </div>
        </form>
    </div>
</div>


@endsection

@push('scripts')
<script>

function makeSlug(){
    var titleSlug = $('.title').val();
    var slugName = titleSlug.split(' ').join('-').toLowerCase();
    $('#slug').val(slugName);
}
</script>


<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>


<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'mytextarea' );
</script>

@endpush