@extends('tutor.app')

@section('title')
    Tambah Tutor
@endsection

@section('content')
    <!-- Content -->
    <div class="card shadow mb-4 col-md-6">
        <div class="card-body">
            <form action="{{ route('tutor.store') }}" method="post">
                @csrf
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="text" class="form-control form-control-user" name="username" placeholder="Username"  value="{{ old('username') }}">
                        @error('username')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="text" class="form-control form-control-user" name="email" placeholder="Email"  value="{{ old('email') }}">
                        @error('email')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="text" class="form-control form-control-user" name="name" placeholder="Nama"  value="{{ old('name') }}">
                        @error('name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="text" class="form-control form-control-user" name="phone" placeholder="No Telp"  value="{{ old('phone') }}">
                        @error('phone')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="password" class="form-control form-control-user" name="password" placeholder="Password"  value="">
                        @error('password')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="password" class="form-control form-control-user" name="password_confirmation" placeholder="Konfirmasi Password"  value="">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-12">
                        <textarea name="address" class="form-control" cols="30" rows="10" placeholder="address"></textarea>
                        @error('address')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-12">
                        <select name="role" class="form-control">
                            <option value="0">Tutor</option>
                            <option value="1">Super Admin</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <input type="submit" value="Tambah Tutor" class="btn btn-primary btn-user btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
