@extends('tutor.app')

@section('title')
Data Pembayaran
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4">
                       
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>                
                        <th>Nama Paket</th>
                        <th>Member</th>
                        <th>Tanggal</th>
                        <th>Bukti Transfer</th>                       
                        <th>Action</th>
                    </tr>
                </thead>
            
                <tbody>
                    @foreach($packets as $row)
                       @foreach(getPayment($row->id) as $data)
                       <tr>                      
                            <td>{{ $row->packet_name }}</td>
                            <td>{{ $data->name }}</td>
                            <td>{{ $data->pivot->payment_date }}</td>
                            <td>@if ($data->pivot->payment_receipt != null) 
                                <a href="{{ asset('storage/file_receipt/'.$data->pivot->payment_receipt) }}" target="_blank"> Download </a>
                                @else 
                                
                                @endif
                            </td>
                            <td>
                                <form action="{{ route('tutor.payment.delete', ['id' => $data->pivot->id]) }}" method="post">
                                
                                @csrf 
                                @method('put')
                            
                                <button type="submit" class="btn btn-danger btn-sm " title="Hapus Pembayaran" onclick="return confirm('yakin hapus data?')"><i class="fa fa-trash"></i></button>
                            </form>
                            </td>
                        </tr>                 
                       @endforeach
                       
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection




