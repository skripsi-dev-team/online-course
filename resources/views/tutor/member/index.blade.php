@extends('tutor.app')

@section('title')
Data Member 
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4">
                       
    <div class="card-body">
        <a href="{{ route('print.member') }}" class="btn btn-secondary"><i class="fa fa-print"></i> Print Data Member</a>
        <br><br>
        <div class="table-responsive">
            <table class="table table-bordered table-category"  width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Member</th>
                        <th>Email</th>
                        <th>No Telp</th>
                        <th>Action</th>
                    </tr>
                </thead>
            
                <tbody>
                    @foreach($members as $key=>$row)
                        
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->phone }}</td>                                    
                            <td>
                                <a href="{{ route('tutor.member.packet', ['id_member' => $row->id ]) }}" class="btn btn-outline-success btn-sm">Lihat Paket</a>
                            </td>
                        </tr>
                        
                    @endforeach
                    
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection

