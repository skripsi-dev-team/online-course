@extends('tutor.app')

@section('title')
Paket Anda yang Dimiliki Oleh {{ $member->name }}
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4">
                       
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Paket</th>
                        <th>Kode Akses</th>
                        <th>Aktivasi</th>
                        <th>Status Paket</th>
                        <th>Action</th>
                    </tr>
                </thead>
            
                <tbody>
                    @foreach($packets as $row)
                        @if($row->tutor_id != Auth::guard('tutor')->user()->id)
                            @continue 
                        @endif
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $row->packet_name }}</td>
                        <td>{{ getKodeAkses($row->id, $member->id) }}</td>
                        <td>{{ checkactivation($row->id, $member->id) }}</td>
                        <td>{{ (checkSertifikat($member->id, $row->id) == 1) ? 'Selesai' : 'Progress' }}</td>
                        <td>
                            <form action="{{ route('send_sertifikat', ['id_member' => $member->id, 'id_paket' => $row->id]) }}" method="post">
                            <a href="{{ route('tutor.member.progress', ['id_member' => $member->id, 'id_paket' => $row->id ]) }}" class="btn btn-outline-success btn-sm progress-btn">Lihat Progress</a>
                            <a href="#" data-target="#sendKode" data-member="{{ $member->id }}" data-packet="{{ $row->id }}" data-toggle="modal" class="btn btn-info btn-sm {{ (checkactivation($row->id, $member->id) == 'Sudah Aktif') ? 'disabled' : '' }} ">Kirim Kode Akses</a>
                            @csrf 
                            @method('PUT')
                           
                            <button type="submit" class="btn btn-primary btn-sm {{ (checkSertifikat($member->id, $row->id) == 1) ? '' : 'disabled'  }}">Kirim Sertifikat</button>
                        </form>
                        </td>
                    </tr>                    
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Kirim activation Kode-->
<div class="modal fade deleteModal" id="sendKode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Kirim activation Kode?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <form action="{{ route('tutor.send-kode') }}" method="post">
                <div class="modal-body">
                    <p class="text-danger">Perhatian!</p>
                    <p>Pengiriman activation kode hanya dilakukan jika member sudah mengkonfirmasi pembayaran. Jika member sudah dipastikan melakukan pembayaran silahkan kirim activation kode ini ke email member dengan cara menekan tombol kirim dibawah</p>                    
                        @csrf 
                        <input type="hidden" name="member_id" class="member_id">
                        <input type="hidden" name="packet_id" class="packet_id">                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary btn-del">Kirim</button>                    
                </div>
            </form>
        </div>
    </div>
</div>

@endsection


@push('scripts')

<script>

$('#sendKode').on('show.bs.modal', function(event){
    var button = $(event.relatedTarget);
    var packet = button.data('packet');
    var member = button.data('member');
    $('.packet_id').val(packet);
    $('.member_id').val(member);

});

</script>

@endpush

