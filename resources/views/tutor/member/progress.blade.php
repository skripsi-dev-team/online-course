@extends('tutor.app')

@section('title')
Progress {{ $member->name }}
@endsection

@section('content')
<!-- Content -->
<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-category" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Kategori</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $progress)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $progress->category_name }}</td>
                        <td>{{ (checkMemberProgress($member->id, $progress->id) == true) ? 'Passed' : 'Not Passed' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- 
<chat-box 
    channel="{{ 'tutor.'.Auth::user()->id }}" 
    event="MemberChatSended" 
    api-url="{{ route('api.chat.send.member', ['tutor' => Auth::user()->id, 'member' => $member->id]) }}"
    history-url="{{ route('api.tutor.history', ['tutor' => Auth::user()->id, 'member' => $member->id]) }}" 
    title="Chat dengan member ini" 
/> -->

@endsection


