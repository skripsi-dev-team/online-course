<style>

body {
    font-family: Arial, Helvetica, sans-serif;
}

.header-print {
    text-align:center;
}
table {
  border-collapse: collapse;
}

table, th, td {
  border: 1px solid black;
}
</style>

<div class="container-print">
    <div class="header-print">
        <h3>TAW Accounting Education Center</h3>
        <p>Raya Padang Luwih No. 2D, Br. Gaji, Dalung, Badung - Bali</p>
        <p>Telfon : +62 857 9207 8189 / +62 821 4703 1969</p>
        <p>Email : taw.course@gmail.com</p>
        <hr>
    </div>
    <div class="body-print">
        <h4>Data Member</h4>

        <table class="table table-bordered table-category"  width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Member</th>
                    <th>Email</th>
                    <th>No Telp</th>
                   
                </tr>
            </thead>
        
            <tbody>
                @foreach($members as $row)
                    
                    <tr>
                        <td style="text-align:center">{{ $loop->iteration }}</td>
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->email }}</td>
                        <td>{{ $row->phone }}</td>                                    
                       
                    </tr>
                    
                @endforeach
                
            </tbody>
        </table>
    </div>
</div>
