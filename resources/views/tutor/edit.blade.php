@extends('tutor.app')

@section('title')
    Edit Tutor
@endsection

@section('content')
    <!-- Content -->
    <div class="card shadow mb-4 col-md-6">
        <div class="card-body">
            <form action="{{ route('tutor.update', $tutor->id) }}" method="post">
                @csrf
                @method('put')
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="text" class="form-control form-control-user" name="username" placeholder="Username"  value="{{ $tutor->username }}">
                        @error('username')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="text" class="form-control form-control-user" name="email" placeholder="Email"  value="{{ $tutor->email }}">
                        @error('email')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="text" class="form-control form-control-user" name="name" placeholder="Nama"  value="{{ $tutor->name }}">
                        @error('name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input type="text" class="form-control form-control-user" name="phone" placeholder="No Telp"  value="{{ $tutor->phone }}">
                        @error('phone')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-12">
                        <textarea name="address" class="form-control" cols="30" rows="10" placeholder="address">{{ $tutor->address }}</textarea>
                        @error('address')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-12">
                        <select name="role" class="form-control">
                            <option value="0" {{ $tutor->role == 0 ? 'selected' : '' }} >Tutor</option>
                            <option value="1" {{ $tutor->role == 1 ? 'selected' : '' }} >Super Admin</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <input type="submit" value="Update Tutor" class="btn btn-primary btn-user btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
