@extends('tutor.app')

@section('title')
Cek Jawaban {{ $test->category->category_name }}
@endsection

@section('content')

<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table" id="dataTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama Member</th>
                        <th>File Jawaban</th>
                        <th>Nilai</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($answers as $jawaban)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $jawaban->date }}</td>
                        <td>{{ $jawaban->member->name }}</td>                        
                        <td><a href="{{ asset('storage/file_answer/'.$jawaban->file_answer) }}">Download</a></td>
                        <td>{{ ($jawaban->score == '') ? 'not set' : $jawaban->score }}</td>
                        <td>{{ ($jawaban->score > 50) ? 'Lulus' : 'Tidak Lulus' }}</td>
                        <td>
                            <button value="{{ $jawaban->id }}"  class="btn btn-outline-info btn-sm btnNilai" data-member="{{ $jawaban->member_id }}" data-toggle="modal" data-target="#nilaiModal">Beri Nilai</button>
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
    
        </div>
    </div>
</div>

<!-- NIALI Modal-->
<div class="modal fade" id="nilaiModal" tabindex="-1" role="dialog" aria-labelledby="nilaiModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailModalLabel">Beri Nilai</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-danger"><b>Note:</b> Nilai dibawah 50 dinyatakan tidak lolos</p>
                <form action="{{ route('answer.nilai') }}" method="post">
                    @csrf 
                    @method('put')
                    <input type="hidden" name="id" class="answer_id" value="">
                    <input type="hidden" name="member_id" class="member_id" value="">
                    <input type="number" name="nilai" class="form-control" value="Masukan nilai" placeholder="Masukan Nilai">
                    <br>
                    <label>Review</label>
                    <textarea name="tutor_description" class="form-control" rows="5"></textarea>
                    <br>
                    <input type="submit" value="Simpan">
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')

<script>

$(document).on('click', '.btnNilai', function(){
    var answer_id = $(this).val();
    $('.answer_id').val(answer_id);
});

$('#nilaiModal').on('show.bs.modal', function(event){
    var button = $(event.relatedTarget);
    $('.member_id').val(button.data('member'));
//    console.log(button.data('member'));
});

</script>

@endpush