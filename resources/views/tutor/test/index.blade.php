@extends('tutor.app')

@section('title')
Data Test {{ $kategori->category_name }}
@endsection

@section('content')
<!-- Content -->

@if($tests == null)
<a href="{{ route('test.create', ['id_kategori' => $kategori->id]) }}" class="btn btn-outline-primary mb-3">Tambah Test Pada Kategori Ini</a>
@endif
<div class="card shadow mb-4">
                       
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
               
                <tbody>
                    @if($tests != null)
                    <tr>
                       <td>File Test</td>
                       <td><a href="{{ asset('storage/file_test/'.$tests->file_test) }}">Download</a></td>
                    </tr>
                    <tr>
                        <td>Deskripsi</td>
                        <td>{!! $tests->description !!}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>{{ $tests->date }}</td>
                    </tr>
                    <tr>
                       <td colspan="2">
                            <a href="{{ route('test.answer', ['id' => $tests->id ]) }}" class="btn btn-outline-info btn-sm">Lihat Jawaban Member</a>
                            <a href="{{ route('test.edit', ['id' => $tests->id]) }}" class="btn btn-warning btn-sm">Edit Test</a>
                       </td>
                    </tr>
                   
                   @else
                    Belum ada test, mohon segera ditambahkan
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection


