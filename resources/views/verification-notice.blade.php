<h1>Your email isn't verified!</h1>
<form action="{{ route('member.logout') }}" method="post">
    @csrf
    <button type="submit" class="btn btn-default">Logout</button>
</form>