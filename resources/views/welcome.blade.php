@extends('member.app')

@section('title')
Selamat Datang
@endsection

@section('content')

    <div class="jumbotron jumbotron-fluid banner">
        <div class="container">
            <h1 class="display-3">TAW Course</h1>
            <p class="lead">Kursus Akuntansi dan Perpajakan di Bali</p>
            <hr class="my-2">
            <p class="lead">
                <a class="btn btn-primary btn-lg" href="{{ url('/member/register') }}" role="button">Daftar Sekarang</a>
            </p>
        </div>
    </div>


<div class="container no-min-height">
    <div class="row mb-5 pt-5 pb-5">
        <div class="col-md-12">
            <h2 class="text-center">Tentang Kami</h2>
            <hr>
        </div>
        <div class="col-md-12 text-center">
            <p><b>Kursus Akuntansi & Perpajakan di Bali.</b></p>
            <p>Kami TAW Course akan membantu Anda baik individu maupun perusahaan dalam pembelajaran / kursus singkat Akutansi & Perpajakan mulai dari materi dasar hingga konsultasi masalah sesuai dengan kebutuhan Anda.</p>
            
        </div>
    </div>
</div>

<div class="container-fluid bg-primary pb-5 pt-5">
    <div class="container ">
        <div class="row text-center">
            <div class="col-md-12">
                <h2 class="text-white">Paket Kursus Kami</h2>
                <hr>
            </div>
        </div>
        <div class="row bg-primary">
            @foreach($packets as $paket)
            <div class="col-md-4 mb-4 mt-4">
                <div class="card">
                    <div class="card-body">
                        <div class="card-text">
                            @if($paket->best_selling == 1)
                            <span class="badge-best">Best Seller</span>
                            @endif
                            <h4 class="text-center">{{ $paket->packet_name }}</h4>
                            <hr>
                            @if(strlen($paket->description) > 300)
                                {!! substr($paket->description, 0, 300) !!}
                            @else 
                                {!! $paket->description !!}
                            @endif
                        </div>
                        <div class="text-center">
                            <h2>Rp. {{ number_format($paket->packet_price) }}</h2>
                        </div>
                        <hr>
                        <div class="text-center">
                            <p>
                                Tutor : <strong>{{ $paket->tutor->name }}</strong>
                            </p>
                            @guest  
                            <a href="/member/register?paket={{ $paket->id }}" class="btn btn-success">Daftar Sekarang</a>
                            @else
                            <a href="#" data-toggle="modal" data-target="#beliModal" data-packet="{{ $paket->id }}" data-member="{{ Auth::guard('member')->user()->id }}" class="btn btn-success">Beli Sekarang</a>
                            @endguest
                            <a href="{{ route('member.packets.detail', ['id' => $paket->id]) }}" class="btn btn-outline-info">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row text-center pt-3 pb-3">
            <div class="col-md-12">
                <a href="{{ url('/packets') }}" class="btn btn-light btn-lg">Lihat Semua Paket</a>
            </div> 
        </div>
    </div>
</div>

<div class="container no-min-height pt-5 pb-5">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center">Software Pendukung</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="image-app mt-3 mb-3">
                <div class="list-image">
                    <img src="{{ asset('image/accounting-150x150.jpg') }}" alt="">
                </div>
                <div class="list-image">
                    <img src="{{ asset('image/myob-150x150.jpg') }}" alt="">
                </div>
                <div class="list-image">
                    <img src="{{ asset('image/xero-150x150.jpg') }}" alt="">
                </div>
                <div class="list-image">
                    <img src="{{ asset('image/vend-150x150.jpg') }}" alt="">
                </div>
                <div class="list-image">
                    <img src="{{ asset('image/taxation-150x150.jpg') }}" alt="">
                </div>
                <div class="list-image">
                    <img src="{{ asset('image/office-150x150.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection